/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

typedef struct {
	byte_t op;
	word_t addr;
	volatile word_t ts;
	volatile word_t ts2;
	cache_t content;
	cache_t backup;
} op_t;

typedef struct {
	char* tag;
	word_t commits;
	word_t aborts;
} hint_t;

typedef struct {
	byte_t  mode;
	byte_t  read_only;
	word_t  nest;
	roll_t* roll;
	hint_t* hint;
	word_t  clocks[zone_count];
	op_t* ops_base;
	op_t* ops_ptr;
	op_t* ops_limit;
} tx_t;

#define LA 1
#define EA 2
#define AA 3

#define READ_OP 1
#define WRITE_OP 2
#define MALLOC_OP 3
#define DEALLOC_OP 4

static __thread tx_t* my_tx;

tx_t* rwset_create() {
	tx_t* result;
	result = malloc(sizeof(tx_t));
	result->ops_base = malloc(sizeof(op_t)*op_count);
	result->ops_ptr = result->ops_base;
	result->ops_limit = result->ops_base + op_count;
	result->read_only = 1;
	result->nest = 0;
	result->roll = 0;
	memset(result->clocks, 0, sizeof(word_t) * zone_count);
	return result;
}

void rwset_free(tx_t* tx) {
	free(tx->ops_base);
	free(tx);
}

tx_t* rwset_locate(void) {
	if (my_tx) return my_tx;
	my_tx = rwset_create();
	return my_tx;
}

void rwset_expand(tx_t* tx) {
	op_t* new_ops;
	word_t new_size, offset;
	new_size = (tx->ops_limit - tx->ops_base) * 2;
	offset = (tx->ops_ptr - tx->ops_base);
	new_ops = realloc(tx->ops_base, new_size * sizeof(op_t));
	if (new_ops == 0) { printf("Read/write set burst!\n"); abort(); }
	tx->ops_ptr = new_ops + offset;
	tx->ops_base = new_ops;
	tx->ops_limit = new_ops + new_size;
}

op_t* rwset_search(tx_t* tx, word_t addr) {
	op_t* ptr;

	/* Find from the back */
	for (ptr = tx->ops_ptr - 1; ptr >= tx->ops_base; --ptr) {
		if (ptr->addr == addr && ptr->op <= WRITE_OP)
			return ptr;
	}

	/* Not found. Is the set full? */
	if (tx->ops_ptr == tx->ops_limit)
		rwset_expand(tx);

	/* Return a vacant slot */
	tx->ops_ptr->addr = 0;
	tx->ops_ptr->op = 0;
	ptr = tx->ops_ptr++;
	return ptr;
}

op_t* rwset_add(tx_t* tx, word_t addr) {
	op_t* ptr;

	/* Is the set full? */
	if (tx->ops_ptr == tx->ops_limit)
		rwset_expand(tx);

	/* Return a vacant slot */
	tx->ops_ptr->addr = 0;
	tx->ops_ptr->op = 0;
	ptr = tx->ops_ptr++;
	return ptr;
}

void rwset_pop(tx_t* tx) {
	--(tx->ops_ptr);
}

byte_t rwset_check(tx_t* tx) {
	op_t* ptr;

	/* Stage 1: load the timestamps in async manner */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != READ_OP) continue;
		mem_timestamp_async(ptr->addr, &ptr->ts2);
	}

	/* Stage 2: read the timestamp */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != READ_OP) continue;
		while (ptr->ts2 == max_word - 1);
		if (ptr->ts2 != ptr->ts) return 0;
	}
	
	/* Every checked to be okay */
	return 1;
}

byte_t rwset_check_all(tx_t* tx) {
	op_t* ptr;

	/* Stage 1: load the timestamps in async manner */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op > WRITE_OP) continue;
		mem_timestamp_async(ptr->addr, &ptr->ts2);
	}

	/* Stage 2: read the timestamp */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op > WRITE_OP) continue;
		while (ptr->ts2 == max_word - 1);
		if (ptr->ts2 != ptr->ts) return 0;
	}
	
	/* Every checked to be okay */
	return 1;
}

byte_t rwset_lock(tx_t* tx) {
	op_t* ptr;

	/* Stage 1: issue the withdraw procedures in async manner */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != WRITE_OP) continue;
		mem_withdraw_async(ptr->addr, ptr->ts, &ptr->ts2);
	}

	/* Stage 2: check the withdraw status */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != WRITE_OP) continue;
		while (ptr->ts2 == max_word - 1);
		if (ptr->ts2 != ptr->ts) goto fail;
	}
	return 1;

	/* Stage 2b: something failed and cancel */
fail:	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != WRITE_OP) continue;
		while (ptr->ts2 == max_word - 1);
		if (ptr->ts2 != ptr->ts) continue;
		mem_deposit(ptr->addr, ptr->ts, ptr->backup);
	}
	return 0;
}

byte_t rwset_unlock(tx_t* tx) {
	op_t* ptr;
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != WRITE_OP) continue;
		mem_deposit(ptr->addr, ptr->ts, ptr->backup);
	}
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != MALLOC_OP) continue;
		alloc_put(ptr->addr, ptr->ts);
	}
	return 1;
}

byte_t rwset_unlock_all(tx_t* tx) {
	op_t* ptr;
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op == READ_OP) {
			mem_deposit(ptr->addr, ptr->ts, ptr->content);
		} else if (ptr->op == WRITE_OP) {
			mem_deposit(ptr->addr, ptr->ts, ptr->backup);
		}
	}
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != MALLOC_OP) continue;
		alloc_put(ptr->addr, ptr->ts);
	}
	return 1;
}

byte_t rwset_release(tx_t* tx, word_t ts) {
	op_t* ptr;
	/* debug("%d: called rwset_release\n", mpi_rank); */
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != WRITE_OP) continue;
		mem_deposit(ptr->addr, ts, ptr->content);
		/* debug("%d: wrote %ld %ld %ld %ld %ld %ld "
		      "%ld %ld onto %ld(%lx)\n",
		      mpi_rank, ptr->content[0], ptr->content[1],
		      ptr->content[2], ptr->content[3], ptr->content[4],
		      ptr->content[5], ptr->content[6], ptr->content[7],
		      ptr->addr, ts); */
	}
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != DEALLOC_OP) continue;
		alloc_put(ptr->addr, ptr->ts);
	}
	return 1;
}

byte_t rwset_release_all(tx_t* tx, word_t ts) {
	op_t* ptr;
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op == READ_OP) {
			mem_deposit(ptr->addr, ptr->ts, ptr->content);
		} else if (ptr->op == WRITE_OP) {
			mem_deposit(ptr->addr, ts, ptr->content);
		}
	}
	for (ptr = tx->ops_base; ptr < tx->ops_ptr; ++ptr) {
		if (ptr->op != DEALLOC_OP) continue;
		alloc_put(ptr->addr, ptr->ts);
	}
	return 1;
}

void rwset_clear(tx_t* tx) {
	tx->ops_ptr = tx->ops_base;
}

void rwset_init(void) {

}

void rwset_exit(void) {

}

