/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static byte_t log2_ceiling(word_t x) {
	byte_t n;
	n = 0;
	while (((word_t)1 << n++) < x);
	return n - 1;
}

word_t* trc_malloc(word_t size) {
	tx_t* tx;
	byte_t power2;
	word_t result, base;
	op_t* op;

	if (size < min_alloc * sizeof(word_t))
		size = min_alloc * sizeof(word_t);
	power2 = log2_ceiling(size + sizeof(word_t) * 2);
	base = alloc_get(power2);
	result = base + sizeof(word_t) * 2;

	/* If we are not running a transaction, just leave without record */
	tx = rwset_locate();
	if (!(tx->nest)) return (word_t*)result;

	/* Add the record to the read / write set */
	op = rwset_add(tx, base);
	op->addr = base;
	op->ts = power2;
	op->op = MALLOC_OP;
	return (word_t*)result;
}

void trc_free(word_t* addr) {
	tx_t* tx;
	byte_t power2;
	word_t ptr, base;
	op_t* op;
	word_t content[line_size];

	base = (word_t)addr - 2 * sizeof(word_t);

	/* If we are not running a transaction, just free it */
	tx = rwset_locate();
	if (!(tx->nest)) {
		alloc_read_or_fail(base, content);
		power2 = alloc_dehash(content[0]);
		/* debug("%d: free %ld (base %ld, size %d)\n",
		      mpi_rank, (word_t)addr, base, power2); */
		alloc_put(base, power2);
		return;
	}

	/* Otherwise, do it transactionally ... */
	/* Write on each cache line for conflict detection */
	power2 = alloc_dehash(trc_read(base));
	/* debug("%d: free %ld (base %ld, size %d)\n",
	      mpi_rank, (word_t)addr, base, power2); */
	for (ptr = (word_t)addr; ptr < (word_t)addr + (1 << power2) - 2;
	     ptr = ptr + sizeof(word_t) + line_size) {
		trc_write((word_t)addr, 0);	
	}
	
	/* Mark the dealloc operation */
	op = rwset_add(tx, base);
	op->addr = base;
	op->ts = power2;
	op->op = DEALLOC_OP;
}

word_t* trc_realloc(word_t* addr, word_t size) {
	return 0;
}

static void malloc_free_init(void) {

}

static void malloc_free_exit(void) {

}

