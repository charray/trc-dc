/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static word_t mem_read(word_t addr, word_t* content) {
	volatile word_t result2;

	if (dir_first(addr) == mpi_rank)
		return cache_read(addr, content);

	result2 = max_word - 1;
	comm_enqueue(READ, dir_first(addr), addr, 0, 0, &result2, content);
	while (result2 == max_word - 1) yield();
	return result2;
}

static word_t mem_timestamp(word_t addr) {
	volatile word_t result2;

	if (dir_first(addr) == mpi_rank)
		return cache_timestamp(addr);

	result2 = max_word - 1;
	comm_enqueue(TS, dir_first(addr), addr, 0, 0, &result2, 0);
	while (result2 == max_word - 1) yield();
	return result2;
}

static void mem_timestamp_async(word_t addr, volatile word_t* ret) {
	if (dir_first(addr) == mpi_rank) {
		*ret = cache_timestamp(addr);
		return;
	}

	*ret = max_word - 1;
	comm_enqueue(TS, dir_first(addr), addr, 0, 0, ret, 0);
	yield();
}

static word_t mem_withdraw(word_t addr, word_t ts) {
	volatile word_t result2;

	if (dir_first(addr) == mpi_rank)
		return cache_withdraw(addr, ts);

	result2 = max_word - 1;
	comm_enqueue(WITHDRAW, dir_first(addr), addr, ts, 0, &result2, 0);
	while (result2 == max_word - 1) yield();
	return result2;
}

static void mem_withdraw_async(word_t addr, word_t ts, volatile word_t* ret) {
	if (dir_first(addr) == mpi_rank) {
		*ret = cache_withdraw(addr, ts);
		return;
	}

	*ret = max_word - 1;
	comm_enqueue(WITHDRAW, dir_first(addr), addr, ts, 0, ret, 0);
}


static word_t mem_rdwithdraw(word_t addr, word_t* content) {
	word_t result;
	volatile word_t result2;

	if (dir_first(addr) == mpi_rank) {
		result = cache_rdwithdraw(addr, content);
		if (result != max_word) return result;
		return max_word;
	}

	result2 = max_word - 1;
	comm_enqueue(RDWITHDRAW, dir_first(addr), addr, 0, 0,
	             &result2, content);
	while (result2 == max_word - 1) yield();
	return result2;
}


static void mem_deposit(word_t addr, word_t ts, word_t* content) {
	word_t result;

	if (dir_first(addr) == mpi_rank) {
		result = cache_deposit(addr, ts, content);
		if (result != max_word) return;
	}

	comm_enqueue(DEPOSIT, dir_first(addr), addr, ts, content, 0, 0);
}

static void mem_test1(void) {
	word_t ts, addr;
	word_t content[line_size];

	for (addr = 4096; addr < mem_count * sizeof(cache_t);
	     addr += sizeof(cache_t) * 62765)
	{
		ts = mem_read(addr, content);
		if (ts == max_word)
			printf("%d: unable to read %ld\n", mpi_rank, addr);
	}
}

static void mem_test2(void) {
	word_t ts, addr;
	word_t content[line_size];

	for (addr = 4096; addr < mem_count * sizeof(cache_t);
	     addr += sizeof(cache_t) * 677723)
	{
retry:		ts = mem_rdwithdraw(addr, content);
		if (ts == max_word) goto retry;
		ts = ts + 1;
		content[0] = content[0] + 1;
		content[4] = content[4] + 2;
		mem_deposit(addr, ts, content);
	}
}

static void mem_test3(void) {
	word_t ts, addr;
	word_t content[line_size];

	for (addr = 4096; addr < mem_count * sizeof(cache_t);
	     addr += sizeof(cache_t) * 677723)
	{
retry:		ts = mem_read(addr, content);
		if (ts == max_word) goto retry;
		if (ts != mpi_size * 2 || content[0] != mpi_size * 2 ||
		    content[4] != mpi_size * 4)
		{
			printf("%d: inconsistent %ld (%ld, %ld, %ld)\n",
			       mpi_rank, addr, ts, content[0], content[4]);
		}
	}

}


void trc_mem_test(void) {
	info("%d: memory unit self test...\n", mpi_rank);
	mem_test1();
	mem_test2();
	mem_test2();
	sleep(20);
	mem_test3();
	sleep(20);
	info("%d: memory unit test completed\n", mpi_rank);
}

static void mem_init(void) {
	/* trc_mem_test(); */
}

static void mem_exit(void) {

}

