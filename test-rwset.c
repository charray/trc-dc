/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "trcdc.h"
#include <stdio.h>
#include <stdlib.h>

#define byte_t unsigned char
#define line_size 8
#define zone_count 256
#define LA 1
#define EA 2
#define AA 3

typedef word_t
	cache_t[line_size];

typedef struct {
	byte_t op;
	word_t addr;
	volatile word_t ts;
	volatile word_t ts2;
	cache_t content;
	cache_t backup;
} op_t;

typedef struct {
	byte_t  mode;
	byte_t  read_only;
	word_t  nest;
	roll_t* roll;
	hint_t* hint;
	word_t  clocks[zone_count];
	op_t* ops_base;
	op_t* ops_ptr;
	op_t* ops_limit;
} tx_t;

tx_t* rwset_locate(void);

trc_word_t* data;
trc_word_t hash(trc_word_t i, trc_word_t j) {
	return i * i + 3 * i + j * j + 9 * j + 13;
}

void test(int mode) {
	trc_word_t i, j;
	trc_roll_t roll;
	printf("%d: Testing with mode %d...\n", trc_rank(), mode);
	fflush(NULL);
	sigsetjmp(roll, 0);
	rwset_locate()->mode = mode;
	for (i = 0; i < 10000; i += 13) {
		trc_begin(&roll, 0);
		for (j = 0; j < i; ++j) {
			trc_write(data + j, hash(i, j));
		}
		for (j = 0; j < i; ++j) {
			if (trc_read(data + j) != hash(i, j)) {
				printf("%d: First-read Failed\n", trc_rank());
				abort();
			}
		}
		trc_commit();

		trc_begin(&roll, 0);
		for (j = 0; j < i; ++j) {
			if (trc_read(data + j) != hash(i, j)) {
				printf("%d: Re-read Failed\n", trc_rank());
				abort();
			}
		}
		trc_commit();
	}
	printf("%d: Done\n", trc_rank());

}

int main(int argc, char** argv) {
	trc_init(&argc, &argv);
	data = trc_malloc(sizeof(trc_word_t) * 1000000);
	trc_barrier();
	test(LA);
	test(EA);
	test(AA);
	printf("%d: Tests completed\n", trc_rank());
	trc_exit();
	return 0;
}
