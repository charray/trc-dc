/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _TRCDC_H_
#define _TRCDC_H_

#include <stdint.h>
#include <setjmp.h>

#define word_t unsigned long
#define quarter_t unsigned short
#define byte_t unsigned char
#define roll_t sigjmp_buf
#define trc_word_t word_t
#define trc_quarter_t quarter_t
#define trc_byte_t byte_t
#define trc_roll_t roll_t
#define trc_hint_t hint_t

typedef struct {
	char* tag;
	word_t commits;
	word_t aborts;
} hint_t;

void trc_init(int*, char***);
void trc_exit(void);
void trc_thread_init(void);
void trc_thread_exit(void);
void trc_begin(roll_t*, hint_t*);
void trc_commit(void);
void trc_abort(void);

word_t trc_read(word_t*);
void trc_write(word_t*, word_t);
void* trc_malloc(word_t);
void* trc_realloc(void*, word_t);
void trc_free(void*);

uint64_t trc_read_u64(uint64_t*);
uint32_t trc_read_u32(uint32_t*);
uint16_t trc_read_u16(uint16_t*);
uint8_t trc_read_u8(uint8_t*);
int64_t trc_read_i64(int64_t*);
int32_t trc_read_i32(int32_t*);
int16_t trc_read_i16(int16_t*);
int8_t trc_read_i8(int8_t*);
double trc_read_double(double*);
float trc_read_float(float*);

void trc_write_u64(uint64_t*, uint64_t);
void trc_write_u32(uint32_t*, uint32_t);
void trc_write_u16(uint16_t*, uint16_t);
void trc_write_u8(uint8_t*, uint8_t);
void trc_write_i64(int64_t*, int64_t);
void trc_write_i32(int32_t*, int32_t);
void trc_write_i16(int16_t*, int16_t);
void trc_write_i8(int8_t*, int8_t);
void trc_write_double(double*, double);
void trc_write_float(float*, float);

byte_t trc_size(void);
byte_t trc_rank(void);
void trc_barrier(void);
void trc_share_pointer(void**, void*);

#define TRC_STRINGIFY(x) #x
#define TRC_TOSTRING(x) TRC_STRINGIFY(x)
#define TRC_AT __FILE__ ":" TRC_TOSTRING(__LINE__)
#define TRC_CONCATENATE_DETAIL(x, y) x##y
#define TRC_CONCATENATE(x, y) TRC_CONCATENATE_DETAIL(x, y)

#define atomic \
trc_roll_t TRC_CONCATENATE(_trc_roll_, __LINE__); \
static trc_hint_t TRC_CONCATENATE(_trc_hint_, __LINE__) = {TRC_AT, 0, 0}; \
trc_word_t TRC_CONCATENATE(_trc_retry_, __LINE__); \
sigsetjmp(TRC_CONCATENATE(_trc_roll_, __LINE__), 0); \
for(TRC_CONCATENATE(_trc_retry_, __LINE__) = 1, trc_begin(&TRC_CONCATENATE(_trc_roll_, __LINE__), &TRC_CONCATENATE(_trc_hint_, __LINE__)); TRC_CONCATENATE(_trc_retry_, __LINE__) == 1; (trc_commit()), TRC_CONCATENATE(_trc_retry_, __LINE__) = 0)

#endif

