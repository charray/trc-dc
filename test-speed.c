/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "trcdc.h"
#include <sys/time.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

word_t* number;

float diff(struct timeval x, struct timeval y) {
	return (x.tv_sec - y.tv_sec) + (x.tv_usec - y.tv_usec) / 1000000.0;
}

void* thread(void* args) {
	int i;
	for (i = 0; i < 10000; ++i) {
		atomic {
			trc_write(number, trc_read(number) + 1);
		}
	}
	return 0;
}

void spawn_threads(void* (*func)(void*), int count) {
	int i; void* dummy;
	pthread_t *threads;

	threads = malloc(sizeof(pthread_t) * count);
	for (i = 0; i < count; ++i)
		pthread_create(threads + i, 0, func, 0);
	for (i = 0; i < count; ++i)
		pthread_join(threads[i], &dummy);
}

int main(int argc, char** argv) {
	struct timeval start, end;
	word_t n, subtotal;

	trc_init(&argc, &argv);

	if (trc_rank() == 0)
		trc_share_pointer(&number, trc_malloc(sizeof(word_t)));

	for (n = 0; n < trc_size(); ++n) {
		trc_barrier();
		if (n != trc_rank()) continue;

		gettimeofday(&start, 0);
		spawn_threads(thread, 4);
		atomic { subtotal = trc_read(number); }
		gettimeofday(&end, 0);
		printf("From %ld: %f per second\n", n, 40000.0/diff(end,start));
		if (subtotal != (n + 1) * 40000)
			printf("%d: Incorrect subtotal\n", trc_rank());
	}

	trc_exit();
	return 0;
}
