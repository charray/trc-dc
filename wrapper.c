/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

typedef union {
	word_t     word[8 / sizeof(word_t)];
	uint64_t   u64[sizeof(cache_t) / sizeof(uint64_t)];
	int64_t    i64[sizeof(cache_t) / sizeof(int64_t)];
	uint32_t   u32[sizeof(cache_t) / sizeof(uint32_t)];
	int32_t    i32[sizeof(cache_t) / sizeof(int32_t)];
	uint16_t   u16[sizeof(cache_t) / sizeof(uint16_t)];
	int16_t    i16[sizeof(cache_t) / sizeof(int16_t)];
	uint8_t    u8[sizeof(cache_t) / sizeof(uint8_t)];
	int8_t     i8[sizeof(cache_t) / sizeof(int8_t)];
	double     d[sizeof(cache_t) / sizeof(double)];
	float      f[sizeof(cache_t) / sizeof(float)];
} convert_t;

#define WORD_BASE(x) \
	(word_t)((word_t)x & ~(word_t)(sizeof(word_t) - 1))

#define SUBWORD(x,y,z) \
	(((word_t)y - (word_t)x) / (word_t)z)

uint64_t trc_read_u64(uint64_t* addr) {
	if (sizeof(word_t) == 8) {
		return trc_read((word_t)addr);
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.u64[0];
	}
}

uint32_t trc_read_u32(uint32_t* addr) {
	if (sizeof(word_t) == 4) {
		return trc_read((word_t)addr);
	} else {
		word_t translated;
		word_t  offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.u32[offset];
	}
}

uint16_t trc_read_u16(uint16_t* addr) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	return convert.u16[offset];
}

uint8_t trc_read_u8(uint8_t* addr) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	return convert.u8[offset];
}

int64_t trc_read_i64(int64_t* addr) {
	if (sizeof(word_t) == 8) {
		return (int64_t)trc_read((word_t)addr);
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.i64[0];
	}
}

int32_t trc_read_i32(int32_t* addr) {
	if (sizeof(word_t) == 4) {
		return (int32_t)trc_read((word_t)addr);
	} else {
		word_t translated;
		word_t  offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.i32[offset];
	}
}

int16_t trc_read_i16(int16_t* addr) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	return convert.i16[offset];
}

int8_t trc_read_i8(int8_t* addr) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	return convert.i8[offset];
}

double trc_read_double(double* addr) {
	if (sizeof(word_t) == 8) {
		convert_t convert;
		convert.word[0] = trc_read((word_t)addr);
		return convert.d[0];
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.d[0];
	}
}

float trc_read_float(float* addr) {
	if (sizeof(word_t) == 4) {
		convert_t convert;
		convert.word[0] = trc_read((word_t)addr);
		return convert.f[0];
	} else {
		word_t translated;
		word_t offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.f[offset];
	}
}

void trc_write_u64(uint64_t* addr, uint64_t value) {
	if (sizeof(word_t) == 8) {
		return trc_write((word_t)addr, value);
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.u64[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}
}

void trc_write_u32(uint32_t* addr, uint32_t value) {
	if (sizeof(word_t) == 4) {
		return trc_write((word_t)addr, value);
	} else {
		word_t translated;
		word_t offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		convert.u32[offset] = value;
		trc_write(translated, convert.word[0]);
	}
}

void trc_write_u16(uint16_t* addr, uint16_t value) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	convert.u16[offset] = value;
	trc_write(translated, convert.word[0]);
}

void trc_write_u8(uint8_t* addr, uint8_t value) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	convert.u8[offset] = value;
	trc_write(translated, convert.word[0]);
}

void trc_write_i64(int64_t* addr, int64_t value) {
	if (sizeof(word_t) == 8) {
		return trc_write((word_t)addr, (word_t)value);
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.i64[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}
}

void trc_write_i32(int32_t* addr, int32_t value) {
	if (sizeof(word_t) == 4) {
		return trc_write((word_t)addr, value);
	} else {
		word_t translated;
		word_t offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		convert.i32[offset] = value;
		trc_write(translated, convert.word[0]);
	}
}

void trc_write_i16(int16_t* addr, int16_t value) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	convert.i16[offset] = value;
	trc_write(translated, convert.word[0]);
}

void trc_write_i8(int8_t* addr, int8_t value) {
	word_t translated;
	word_t offset;
	convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	convert.i8[offset] = value;
	trc_write(translated, convert.word[0]);
}

void trc_write_double(double* addr, double value) {
	if (sizeof(word_t) == 8) {
		convert_t convert;
		convert.d[0] = value;
		trc_write((word_t)addr, convert.word[0]);
	} else {
		convert_t convert;
		word_t translated;
		translated = (word_t)addr;
		convert.d[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}

}

void trc_write_float(float* addr, float value) {
	if (sizeof(word_t) == 4) {
		convert_t convert;
		convert.f[0] = value;
		trc_write((word_t)addr, convert.word[0]);
	} else {
		word_t translated;
		word_t  offset;
		convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		convert.f[offset] = value;
		trc_write(translated, convert.word[0]);
	}
}

void trc_share_pointer(void** ptr, void* content) {
	byte_t i;
	for (i = 0; i < mpi_size; ++i) {
		if (i == mpi_rank) continue;
		comm_enqueue(RETURN, i, 0, (word_t)content, 0, (word_t*)ptr, 0);
	}
	*ptr = content;
}

byte_t trc_size(void) {
	return mpi_size;
}

byte_t trc_rank(void) {
	return mpi_rank;
}

void trc_barrier(void) {
	byte_t i;

	/* Special case: no neighbours. Just return. */
	if (mpi_size == 1) return;

	/* daemon_counter3 is incremented by one whenever it collected
	   enough BARRIER messages from neighbours. We increment
	   the counter2 and wait for counter3 to be at the same value */
	daemon_counter2++;
	for (i = 0; i < mpi_size; ++i) {
		if (i == mpi_rank) continue;
		comm_enqueue(BARRIER, i, daemon_counter2, 0, 0, 0, 0);
	}
	while (daemon_counter3 < daemon_counter2) sched_yield();
}

void wrapper_init(void) {

}

void wrapper_exit(void) {

}

