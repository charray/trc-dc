#include "trcdc.h"
#include <iostream>
#include <map>
using namespace std;

namespace trcdc {

class l {
public:
	void* s; // size
	void* c; // content
	void* b; // backup
};

__thread map<void*, l>* log;

template<class T> class s {
private:
	T* a;
public:
	s<T>() { a = 0; }
	s<T>(void* addr) { a =(T*)addr; }
	s<T>(const s<T>& o) { a = o.a; }
	s<T>& operator= (void* addr) { a = (T*)addr; }
	s<T>& operator= (const s<T>& o) { a = o.a; }

	s<T>& operator++ () { ++a; return *this; }
	s<T> operator++ (int) { s<T> r(*this); ++a; return r; }
	s<T>& operator-- () { --a; return *this; } 
	s<T> operator-- (int) { s<T> r(*this); --a; return r; }

	s<T>& operator+= (word_t x) { a += x; }
	s<T>& operator-= (word_t x) { a -= x; }
	s<T> operator+ (word_t x) const { s<T> r(*this); r += x; return r; }
	s<T> operator- (word_t x) const { s<T> r(*this); r -= x; return r; }

	bool operator< (const s<T>& o) const { return a < o.a; }
	bool operator> (const s<T>& o) const { return a > o.a; }
	bool operator<= (const s<T>& o) const { return a <= o.a; }
	bool operator>= (const s<T>& o) const { return a >= o.a; }
	bool operator!= (const s<T>& o) const { return a != o.a; }
	bool operator== (const s<T>& o) const { return a == o.a; }

	T& operator* () const { return a[0]; }
	T* operator-> () const { return &a[0]; }
	T& operator[] (word_t x) const { return a[x]; }

};

}

using namespace trcdc;

struct b {
	int a;
	int d;
	s<b> next;
};

int main(int argc, char** argv) {
	s<int> x = malloc(sizeof(int) * 4);
	s<s<int> > y = malloc(sizeof(int*) * 4);
	s<b> z = malloc(sizeof(b));

	*x = 10;
	x[1] = 20;
	x[2] = 30;
	x[3] = 40;
	cout << *x << " " << *(x+1) << " " << *(x-2+4) << " " << x[3] << endl;

	y[0] = malloc(sizeof(int));
	y[1] = malloc(sizeof(int) * 2);
	*y[0] = 15;
	y[1][0] = 25;
	y[1][1] = 35;
	cout << **y << " " << *y[1] << " " << y[1][1] << endl;

	z->a = 19;
	z->d = 29;
	z->next = malloc(sizeof(b));
	z->next->a = 39;
	cout << z->a << " " << (*z).d << " " << z->next->a << endl;
}

