/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define node(x) ((word_t)x >> ((sizeof(word_t) - sizeof(byte_t)) * 8))
#define newts(x) ((word_t)x << ((sizeof(word_t) - sizeof(byte_t)) * 8))

static volatile word_t timestamp_pool[max_byte];

void timestamp_share(byte_t node, word_t content) {
	word_t temp;
	while (1) {
		temp = timestamp_pool[node];
		if (temp >= content) return;
		if (cas(&timestamp_pool[node], temp, content)) return;
	}
}

void timestamp_get(word_t* content) {
	byte_t i;
	for (i = 0; i < mpi_size; ++i) content[i] = timestamp_pool[i];
}

word_t timestamp_new(void) {
	word_t result;
	result = inc(&timestamp_pool[mpi_rank], 1);
	if (node(result) != mpi_rank) { printf("Timestamp overflow"); abort(); }
	return result;
}

void timestamp_init(void) {
	byte_t i;
	for (i = 0; i < max_byte; ++i) {
		timestamp_pool[i] = newts(i);
	}
}

void timestamp_exit(void) {

}

