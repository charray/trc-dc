/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <mpi.h>
#include <stdint.h>
#include <setjmp.h>
#include <sched.h>
#include <sys/time.h>

#include "config.h"
#include "mpi.c"
#include "comm.c"
#include "dir.c"

#ifdef migration
#include "overflow.c"
#endif

#include "cache.c"

#ifdef migration
#include "mem-migrate.c"
#else
#include "mem.c"
#endif

#include "alloc.c"

#ifdef migration
#include "daemon.c"
#else
#include "daemon-migrate.c"
#endif

#include "rwset.c"
#include "timestamp.c"
#include "begin_commit.c"
#include "read_write.c"
#include "malloc_free.c"
#include "wrapper.c"

void trc_init(int* argc, char*** argv) {
	mpi_init(argc, argv);
	comm_init();
	dir_init();
#ifdef migration
	overflow_init();
#endif
	cache_init();
	daemon_init();
	mem_init();
	alloc_init();
	timestamp_init();
	
	rwset_init();
	read_write_init();
	malloc_free_init();
	wrapper_init();
}

void trc_exit(void) {
	wrapper_exit();
	malloc_free_exit();
	read_write_exit();
	rwset_exit();

	timestamp_exit();
	alloc_exit();
	mem_exit();
	daemon_exit();
	cache_exit();
#ifdef migration
	overflow_exit();
#endif
	dir_exit();
	comm_exit();
	mpi_exit();
}

