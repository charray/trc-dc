/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static pthread_t daemon_thread;
static byte_t daemon_continue;

static byte_t daemon_counter;
static word_t daemon_counter2;
static volatile word_t daemon_counter3;

static void daemon_ts(const queue_t* req) {
	word_t result;
	result = cache_timestamp(req->addr);
	if (result != max_word) {
		comm_enqueue(RETURN, req->serv, req->addr,
		             result, 0, req->ts_ptr, 0);
		return;
	}
	
	comm_enqueue(RETURN, req->serv, req->addr,
	             max_word, 0, req->ts_ptr, 0);
}

static void daemon_read(const queue_t* req) {
	word_t result;
	word_t content[line_size];
	result = cache_read(req->addr, content);
	if (result != max_word) {
		comm_enqueue(CACHE, req->serv, req->addr,
		             result, content, req->ts_ptr,
			     req->content_ptr);
		return;
	}

	comm_enqueue(RETURN, req->serv, req->addr,
	             max_word, 0, req->ts_ptr, 0);
}

/*
static void daemon_fwread(const queue_t* req) {
	word_t result;
	word_t content[line_size];
	result = cache_read(req->addr, content);
	if (result != max_word) {
		comm_enqueue(CACHE, req->serv, req->addr,
		             result, content, req->ts_ptr,
			     req->content_ptr);
		return;
	}

	comm_enqueue(RETURN, req->serv, req->addr,
	             max_word, 0, req->ts_ptr, 0);
}
*/

static void daemon_withdraw(const queue_t* req) {
	word_t result;
	result = cache_withdraw(req->addr, req->ts);
	comm_enqueue(RETURN, req->serv, req->addr,
		     result, 0, req->ts_ptr, 0);
}

static void daemon_rdwithdraw(const queue_t* req) {
	word_t result;
	word_t content[line_size];
	result = cache_rdwithdraw(req->addr, content);
	if (result != max_word) {
		comm_enqueue(CACHE, req->serv, req->addr,
		             result, content, req->ts_ptr,
			     req->content_ptr);
		return;
	}

	comm_enqueue(RETURN, req->serv, req->addr,
	             result, 0, req->ts_ptr, 0);
}

static void daemon_abandon(const queue_t* req) {

}

static void daemon_deposit(const queue_t* req) {
	word_t result;
	result = cache_deposit(req->addr, req->ts, 
	                       req->content);
}

static void daemon_malloc(const queue_t* req) {
	word_t result;

	/* alloc_get() and deamon_malloc() forward the request to
	   the mpi_next repeatedly when there are not suitable slots.
	   If we end up receiving the request from ourself,
	   that is nobody has the memory we need */
	if (req->serv == mpi_rank) {
		*(req->ts_ptr) = 0;
		return;
	}

	/* Request memory. Note we ask the allocator not to forward request
	   to other nodes as we will handle it separately.
	   The daemon_queue is under use now (for obvious reasons),
	   allowing the allocator to wait for another daemon RETURN
	   would end up in dead lock. */
	result = alloc_get_impl(req->addr, 0);
	if (result)
		comm_enqueue(RETURN, req->serv, 0,
		             result, 0, req->ts_ptr, 0);
	else
		comm_forward(MALLOC, mpi_next, req->serv, req->addr,
		             0, 0, req->ts_ptr, 0);
}

static void daemon_dealloc(const queue_t* req) {
	alloc_put(req->addr, req->ts);
}

static void daemon_barrier(const queue_t* req) {
	if (--daemon_counter == 0) {
		daemon_counter3 += 1;
		daemon_counter = mpi_size - 1;
	}
}

static void daemon_handle(byte_t peer, const queue_t* req) {
	switch (req->action) {
	case TS: return daemon_ts(req);
	case READ: return daemon_read(req);
	/*case REREAD: return daemon_fwread(req);*/
	case WITHDRAW: return daemon_withdraw(req);
	case RDWITHDRAW: return daemon_rdwithdraw(req);
	case ABANDON: return daemon_abandon(req);
	case DEPOSIT: return daemon_deposit(req);
	case RETURN:
		*(req->ts_ptr) = req->ts;
		break;
	case CACHE:
		memcpy(req->content_ptr, req->content, sizeof(cache_t));
		*(req->ts_ptr) = req->ts;
		break;
	case MALLOC: return daemon_malloc(req);
	case DEALLOC: return daemon_dealloc(req);
	case STOP: --daemon_continue; break;
	case BARRIER: return daemon_barrier(req);
	default: break;
	}
}

static void* daemon_function(void* args) {
	int length, offset, i, j;
	byte_t peer;
	word_t msg[msg_max];
	queue_t req[cq_length];
	MPI_Status status;

	daemon_continue = mpi_size;
	while (daemon_continue) {
		length = i = offset = 0;
		MPI_Recv(msg, msg_max * sizeof(word_t), MPI_BYTE,
		         MPI_ANY_SOURCE, 0, mpi_world, &status);
		MPI_Get_count(&status, MPI_BYTE, &length);
		length = length / sizeof(word_t);
		peer = status.MPI_SOURCE;

		while (offset != length && msg[offset] != 0)
			offset = comm_extract(&req[i++], msg, offset);

		for (j = 0; j < i; ++j) daemon_handle(peer, &req[j]);
		yield();
	}
	return 0;
}

static void daemon_init(void) {
	daemon_counter = mpi_size - 1;
	daemon_counter2 = 0;
	daemon_counter3 = 0;
	pthread_create(&daemon_thread, 0, daemon_function, 0);
	info("%d: daemon initialised\n", mpi_rank);
}

static void daemon_exit(void) {
	int i;
	void* dummy;
	for (i = 0; i < mpi_size; ++i) comm_enqueue(STOP, i, 0, 0, 0, 0, 0);
	pthread_join(daemon_thread, &dummy);
}

