/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static volatile cache_t* overflow_cache;
static volatile meta_t* overflow_meta;

/* cache .addr convention
   0: empty
   addr: address (plain address, unlike cache)
   max_word - 1: deleted
   max_word: locked

   Unlike the cache unit, we do not have incarnation number here.
   In fact incarnation helps reading one less number in checking,
   but we just do not have enough space as we need to record all the bits
   in an address
*/

#define hash(x)         (num(x) % (over_count / cache_assoc))
#define hash2(x, y)     ((y + num(x) % 23677 + 237) % over_count)

/* Find and lock functions */
static word_t overflow_find(word_t addr, word_t* ts) {
	word_t pos, meta_addr;
	
	pos = hash(addr);
	while (1) {
		/* If found, return it */
retry:		meta_addr = overflow_meta[pos].addr;
		if (addr == meta_addr) {
			*ts = overflow_meta[pos].ts;
			meta_addr = overflow_meta[pos].addr;
			if (addr != meta_addr) goto retry;
			return pos;
		}

		/* Next iteration */
		if (meta_addr == 0) break;
		pos = hash2(addr, pos);
		if (pos == hash(addr)) break;
	}

	return over_count;
}

static word_t overflow_lock(word_t addr) {
	word_t pos, meta_addr;

	pos = hash(addr);
	while (1) {
		/* If found, lock and return it */
		meta_addr = overflow_meta[pos].addr;
		if (addr == meta_addr &&
		    cas(&overflow_meta[pos].addr, addr, max_word))
		{
			return pos;
		}

		/* Next iteration */
		if (meta_addr == 0) break;
		pos = hash2(addr, pos);
		if (pos == hash(addr)) break;
	}

	return over_count;
}

static word_t overflow_lock_free(word_t addr) {
	word_t pos, meta_addr;

	pos = hash(addr);
	while (1) {
		/* If found, lock and return */
		meta_addr = overflow_meta[pos].addr;
		if ((meta_addr == 0 || meta_addr == max_word - 1) &&
		    cas(&overflow_meta[pos].addr, meta_addr, max_word))
		{
			return pos;
		}

		/* Next iteration */
		pos = hash2(addr, pos);
		if (pos == hash(addr)) break;

	}
	return over_count;
}

/* Verify and release functions */
static byte_t overflow_verify(word_t pos, word_t addr, word_t ts) {
	return (overflow_meta[pos].addr == addr &&
	        overflow_meta[pos].ts == ts);
}

static void overflow_release_fast(word_t pos, word_t addr) {
	overflow_meta[pos].addr = addr;
}

static void overflow_release_clean(word_t pos) {
	memset((void*)overflow_cache[pos], 0, sizeof(cache_t));
	overflow_meta[pos].ts = 0;
	overflow_meta[pos].addr = max_word_bit - 1;
}

static void overflow_release(word_t pos, word_t addr, word_t ts, const word_t* content) {
	memcpy((void*)overflow_cache[pos], content, sizeof(cache_t));
	overflow_meta[pos].ts = ts;
	overflow_meta[pos].addr = addr;
}

/* Interface for getting and putting cache content */
static word_t overflow_read(word_t addr, word_t* content) {
	word_t pos, ts;

retry:	pos = overflow_find(addr, &ts);
	if (pos == over_count) {
		return max_word;
	}

	memcpy(content, (void*)overflow_cache[pos], sizeof(cache_t));

	if (!overflow_verify(pos, addr, ts)) {
		goto retry;
	}

	return ts;
}

static word_t overflow_timestamp(word_t addr) {
	word_t pos, ts;

	pos = overflow_find(addr, &ts);
	return ts;
}

static word_t overflow_withdraw(word_t addr, word_t ts) {
	word_t pos, result;

	pos = overflow_lock(addr);
	if (pos == over_count) {
		return max_word;
	}

	result = overflow_meta[pos].ts;
	if (result != ts) {
		overflow_release_fast(pos, addr);
		return result;
	}

	if (!dir_cas(addr, OVERFLOW_EXCLUSIVE, WITHDRAWN)) {
		overflow_release_fast(pos, addr);
		return max_word - 1;
	}

	overflow_release_clean(pos);
	return ts;
}

static word_t overflow_rdwithdraw(word_t addr, word_t* content) {
	word_t pos, result;

	pos = overflow_lock(addr);
	if (pos == over_count) {
		return max_word;
	}

	if (!dir_cas(addr, OVERFLOW_EXCLUSIVE, WITHDRAWN)) {
		overflow_release_fast(pos, addr);
		return max_word - 1;
	}

	memcpy(content, (void*)overflow_cache[pos], sizeof(cache_t));
	result = overflow_meta[pos].ts;
	overflow_release_clean(pos);
	return result;
}

static word_t overflow_deposit(word_t addr, word_t ts, const word_t* content) {
	word_t pos;
	pos = overflow_lock_free(addr);
	if (pos == over_count) return max_word;

	if (!dir_cas(addr, WITHDRAWN, OVERFLOW_EXCLUSIVE)) {
		overflow_release_clean(pos);
		return max_word;
	}

	overflow_release(pos, addr, ts, content);
	return ts;
}

void trc_overflow_test() {
	word_t i, ts;
	cache_t content;

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i += sizeof(cache_t)) {
		dir_set(i, WITHDRAWN);
		ts = i;
		content[0] = i;
		content[2] = i * i;
		content[7] = i * i - i;
		overflow_deposit(i, ts, content);
		if (i%1000000*sizeof(cache_t)==0)debug("%d: d%ld\n",mpi_rank,i);
	}

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i += sizeof(cache_t)) {
		ts = overflow_read(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d: mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d: r%ld\n",mpi_rank,i);
	}

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i += sizeof(cache_t)) {
		ts = overflow_rdwithdraw(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d: mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d: w%ld\n",mpi_rank,i);
	}
}

void* trc_overflow_test2_thread1(void* whatsoeverstupid) {
	word_t i, ts;
	cache_t content;

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i+=sizeof(cache_t)*2) {
		dir_set(i, WITHDRAWN);
		ts = i;
		content[0] = i;
		content[2] = i * i;
		content[7] = i * i - i;
		overflow_deposit(i, ts, content);
		if (i%1000000*sizeof(cache_t)==0)debug("%d: d%ld\n",mpi_rank,i);
	}

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i+=sizeof(cache_t)*2) {
		ts = overflow_read(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d: mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d: r%ld\n",mpi_rank,i);
	}

	for (i=4096; i < 4096+10000000*sizeof(cache_t); i+=sizeof(cache_t)*2) {
		ts = overflow_rdwithdraw(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d: mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d: w%ld\n",mpi_rank,i);
	}

	return 0;
}

void* trc_overflow_test2_thread2(void* whatsoeverstupid) {
	word_t i, ts;
	cache_t content;

	for (i=4096 + sizeof(cache_t);
	     i < 4096+10000000*sizeof(cache_t); i+=sizeof(cache_t)*2) {
		dir_set(i, WITHDRAWN);
		ts = i;
		content[0] = i;
		content[2] = i * i;
		content[7] = i * i - i;
		overflow_deposit(i, ts, content);
		if (i%1000000*sizeof(cache_t)==0)debug("%d- d%ld\n",mpi_rank,i);

		ts = overflow_read(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d- mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d- r%ld\n",mpi_rank,i);

		ts = overflow_rdwithdraw(i, content);
		if (ts != i ||
		    content[0] != i ||
		    content[2] != i * i ||
		    content[7] != i * i - i)
		{
			printf("%d- mismatch %ld\n", mpi_rank, i);
		}
		if (i%1000000*sizeof(cache_t)==0)debug("%d- w%ld\n",mpi_rank,i);
	}

	return 0;
}

void trc_overflow_test2(void) {
	pthread_t partner;
	void* dummy;
	pthread_create(&partner, 0, trc_overflow_test2_thread1, 0);
	trc_overflow_test2_thread2(0);
	pthread_join(partner, &dummy);
}

/* Self test procedures */
/* Init and terminate functions */
static void overflow_init(void) {
	cache_t empty;

	/* Allocate the memory */
	overflow_meta = malloc(sizeof(meta_t) * over_count);
	overflow_cache = malloc(sizeof(cache_t) * over_count);
	memset(empty, 0, sizeof(cache_t));

	/* We do not initialize the meta content as they would be zero
	   from the operating system. */
	info("%d: overflow cache initialised\n", mpi_rank);

	info("%d: testing overflow unit... \n", mpi_rank);
	trc_overflow_test2();
	info("%d: overflow test completed.\n", mpi_rank);
}

static void overflow_exit(void) {
	free((void*)overflow_cache);
	free((void*)overflow_meta);
}

