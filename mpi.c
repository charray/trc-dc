/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static MPI_Comm mpi_world;
static int mpi_rank;
static int mpi_next;
static int mpi_size;
static char mpi_name[MPI_MAX_PROCESSOR_NAME];

static void mpi_init(int* argc, char*** argv) {
	int dummy;
	MPI_Comm comm;
	MPI_Group group;
	cpu_set_t affinity;

	MPI_Init(argc, argv);
	MPI_Comm_dup(MPI_COMM_WORLD, &comm);
	MPI_Comm_group(MPI_COMM_WORLD, &group);
	MPI_Comm_create(comm, group, &mpi_world);

	MPI_Comm_rank(mpi_world, &mpi_rank);
	MPI_Comm_size(mpi_world, &mpi_size);
	MPI_Get_processor_name(mpi_name, &dummy);

	/* I hearby curse the one who set affinity without my prior
	   consent! Why the freak I am locked to use one processors
	   for all my N threads! */
	CPU_ZERO(&affinity);
	for (dummy = 0; dummy < sysconf(_SC_NPROCESSORS_ONLN); ++dummy)
		CPU_SET(dummy, &affinity);
	sched_setaffinity(0, sizeof(cpu_set_t), &affinity);

	mpi_next = (mpi_rank + 1) % mpi_size;
	info("%s (%d/%d) MPI initialised\n", mpi_name, mpi_rank, mpi_size);
}

static void mpi_exit(void) {
	info("%s (%d/%d) MPI finishes\n", mpi_name, mpi_rank, mpi_size);
	MPI_Finalize();
}

