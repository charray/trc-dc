/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "trcdc.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define panic() { printf("fault at line %d\n", __LINE__); abort(); }

typedef struct node {
	int value;
	struct node* parent;
	struct node* left;
	struct node* right;
} node_t;

node_t* root;

void print_internal(node_t* node) {
	if (node == 0) return;
	print_internal((node_t*)trc_read_u64((uint64_t*)&node->left));
	printf(" %d", trc_read_i32(&node->value));
	print_internal((node_t*)trc_read_u64((uint64_t*)&node->right));
}

void print(node_t* node) {
	atomic {
		print_internal(node);
	}
	printf("\n");
}

int hash(int x) {
	return ((long)x * 1577999L) % 1000000L + 1;
}

int hash2(int x) {
	return ((long)x * 1488337L) % 1000000L + 1;
}

void add(node_t* head, int value) {
	node_t* new_node;
	int read_value;
	node_t* read_left;
	node_t* read_right;

	read_value = trc_read_i32(&head->value);
	read_left = (node_t*)trc_read_u64((uint64_t*)&head->left);
	read_right = (node_t*)trc_read_u64((uint64_t*)&head->right);

	/* Consider adding under child nodes */
	if (value == read_value) return;
	if (value < read_value && read_left) return add(read_left, value);
	if (read_value < value && read_right) return add(read_right, value);

	/* Child node absent. Insert here */
	new_node = trc_malloc(sizeof(node_t));
	trc_write_i32(&new_node->value, value);
	trc_write_u64((uint64_t*)&new_node->parent, (uint64_t)head);
	trc_write_u64((uint64_t*)&new_node->left, 0);
	trc_write_u64((uint64_t*)&new_node->right, 0);
	if (value < read_value)
		trc_write_u64((uint64_t*)&head->left, (uint64_t)new_node);
	if (read_value < value)
		trc_write_u64((uint64_t*)&head->right, (uint64_t)new_node);
}

void delete(node_t* head, int value) {
	int read_value;
	node_t* read_left;
	node_t* read_right;
	node_t* read_parent;
	node_t* replacement;

	read_value = trc_read_i32(&head->value);
	read_left = (node_t*)trc_read_u64((uint64_t*)&head->left);
	read_right = (node_t*)trc_read_u64((uint64_t*)&head->right);

	/* Consider deleting child node */
	if (value < read_value && read_left) return delete(read_left, value);
	if (read_value < value && read_right) return delete(read_right, value);
	if (value != read_value) return;

	/* So now we know we need to delete head */
	if (read_left && read_right) goto trouble;
	else if (read_left && !read_right) replacement = read_left;
	else if (!read_left && read_right) replacement = read_right;
	else if (!read_left && !read_right) replacement = 0;

	read_parent = (node_t*)trc_read_u64((uint64_t*)&head->parent);
	if (replacement)
	trc_write_u64((uint64_t*)&replacement->parent, (uint64_t)read_parent);

	if (trc_read_u64((uint64_t*)&read_parent->left) == (uint64_t)head)
	trc_write_u64((uint64_t*)&read_parent->left, (uint64_t)replacement);
	else if (trc_read_u64((uint64_t*)&read_parent->right) == (uint64_t)head)
	trc_write_u64((uint64_t*)&read_parent->right, (uint64_t)replacement);
	else panic();

	trc_free(head);
	return;

trouble:
	replacement = read_right;
	while ((read_left=(node_t*)trc_read_u64((uint64_t*)&replacement->left)))
		replacement = read_left;
	read_value = trc_read_i32(&replacement->value);
	delete(head, read_value);
	trc_write_i32(&head->value, read_value);
}

void init() {
	if (trc_rank() == 0) {
		atomic {
			root = trc_malloc(sizeof(node_t));
			trc_write_i32(&root->value, 0);
			trc_write_u64((uint64_t*)&root->parent, 0);
			trc_write_u64((uint64_t*)&root->left, 0);
			trc_write_u64((uint64_t*)&root->right, 0);
		}
		trc_share_pointer((void**)&root, root);
	}
}

void* test(void* arg) {
	volatile int i, j;
	for (i = 1; i < 500000; i += 10) {
		atomic {
			for (j = 0; j < 10; ++j) {
				add(root, hash(i + j));
			}
		}
		if (i % 10000 == 1) printf("%d: i %d\n", trc_rank(), i);
	}

	for (i = 1; i < 500000; i += 10) {
		atomic {
			for (j = 0; j < 10; ++j) {
				delete(root, hash(i + j));
			}
		}
		if (i % 10000 == 1) printf("%d: d %d\n", trc_rank(), i);
	}
	return 0;
}

void* test2(void* arg) {
	volatile int i, j;
	for (i = 500000; i < 1000000; i += 50) {
		atomic {
			for (j = 0; j < 10; ++j) {
				add(root, hash(i + j));
			}
		}
		if (i % 10000 == 0) printf("%d: i %d\n", trc_rank(), i);
	}

	for (i = 500000; i < 1000000; i += 50) {
		atomic {
			for (j = 0; j < 10; ++j) {
				delete(root, hash(i + j));
			}
		}
		if (i % 10000 == 0) printf("%d: d %d\n", trc_rank(), i);
	}
	return 0;
}


int main(int argc, char** argv) {
	trc_init(&argc, &argv);
	init();

	trc_barrier();
	printf("%d running...\n", trc_rank());
	if (trc_rank() == 0) test(0);
	if (trc_rank() == 1) test2(0);
	printf("%d waiting...\n", trc_rank());
	trc_barrier();

	if (trc_rank() == 0) {
		printf("after delete:");
		print(root);
	}

	trc_exit();
	return 0;
}
