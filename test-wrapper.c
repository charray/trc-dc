/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "trcdc.h"
#include <stdio.h>

char small_hash(long long x) {
	return (x * x + x * 2) % 256;
}

void test_uint64() {
	uint64_t* data;
	uint64_t read_value;
	uint64_t i;
	trc_roll_t roll;

	data = (uint64_t*)trc_malloc(100000 * sizeof(uint64_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_u64(data + i, small_hash(i));
		read_value = trc_read_u64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing uint64\n");
}

void test_int64() {
	int64_t* data;
	int64_t read_value;
	int64_t i;
	trc_roll_t roll;

	data = (int64_t*)trc_malloc(100000 * sizeof(int64_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_i64(data + i, small_hash(i));
		read_value = trc_read_i64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i64(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %ld, %d vs %ld\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing int64\n");
}

void test_uint32() {
	uint32_t* data;
	uint32_t read_value;
	uint32_t i;
	trc_roll_t roll;

	data = (uint32_t*)trc_malloc(100000 * sizeof(uint32_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_u32(data + i, small_hash(i));
		read_value = trc_read_u32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing uint32\n");
}

void test_int32() {
	int32_t* data;
	int32_t read_value;
	int32_t i;
	trc_roll_t roll;

	data = (int32_t*)trc_malloc(100000 * sizeof(int32_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_i32(data + i, small_hash(i));
		read_value = trc_read_i32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i32(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing int32\n");
}

void test_uint16() {
	uint16_t* data;
	uint16_t read_value;
	int i;
	trc_roll_t roll;

	data = (uint16_t*)trc_malloc(100000 * sizeof(uint16_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_u16(data + i, small_hash(i));
		read_value = trc_read_u16(data + i);
		if ((uint16_t)read_value != (uint16_t)small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u16(data + i);
		if ((uint16_t)read_value != (uint16_t)small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u16(data + i);
		if ((uint16_t)read_value != (uint16_t)small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing uint16\n");
}

void test_int16() {
	int16_t* data;
	int16_t read_value;
	int i;
	trc_roll_t roll;

	data = (int16_t*)trc_malloc(100000 * sizeof(int16_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_i16(data + i, small_hash(i));
		read_value = trc_read_i16(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i16(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i16(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing int16\n");
}

void test_uint8() {
	uint8_t* data;
	uint8_t read_value;
	int i;
	trc_roll_t roll;

	data = (uint8_t*)trc_malloc(100000 * sizeof(uint8_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_u8(data + i, small_hash(i));
		read_value = trc_read_u8(data + i);
		if ((uint8_t)read_value != (uint8_t)small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u8(data + i);
		if ((uint8_t)read_value != (uint8_t)small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_u8(data + i);
		if ((uint8_t)read_value != (uint8_t)small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing uint8\n");
}

void test_int8() {
	int8_t* data;
	int8_t read_value;
	int i;
	trc_roll_t roll;

	data = (int8_t*)trc_malloc(100000 * sizeof(int8_t));
	sigsetjmp(roll, 0);
	trc_begin(&roll, 0);
	for (i = 0; i < 100000; ++i) {
		trc_write_i8(data + i, small_hash(i));
		read_value = trc_read_i8(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch immediate: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i8(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch within: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_commit();
	for (i = 0; i < 100000; ++i) {
		read_value = trc_read_i8(data + i);
		if (read_value != small_hash(i)) {
			printf("Mismatch after commit: %d, %d vs %d\n",
				i, small_hash(i), read_value);
		}
	}
	trc_free(data);
	printf("Success testing int8\n");
}

int main(int argc, char** argv) {
	trc_init(&argc, &argv);
	test_uint64();
	test_int64();
	test_uint32();
	test_int32();
	test_uint16();
	test_int16();
	test_uint8();
	test_int8();
	trc_exit();
	return 0;
}
