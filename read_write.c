/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

word_t trc_read_aa(tx_t* tx, word_t addr) {
	op_t* ptr;
	byte_t offset;

	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	// debug("%d: read %ld\n", mpi_rank, addr);
	ptr = rwset_search(tx, addr);

	/* Found on previous read/write record */
	if (ptr->addr) {
		return ptr->content[offset];
	}

	/* Not found. Trying to lock it now */
	ptr->addr = addr;
	ptr->ts = mem_rdwithdraw(addr, ptr->content);
	ptr->op = READ_OP;

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_aa(tx);
	}

	/* Well done */
	return ptr->content[offset];
}

word_t trc_read_ea(tx_t* tx, word_t addr) {
	op_t* ptr;
	byte_t offset;
	
	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	ptr = rwset_search(tx, addr);

	/* Found on previous read/write record */
	if (ptr->addr) {
		return ptr->content[offset];
	}

	/* Not found. Trying to read it now */
	ptr->addr = addr;
	ptr->ts = mem_read(addr, ptr->content);
	ptr->op = READ_OP;

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_ea(tx);
	}

	/* Do we need to extend? */
	if (ptr->ts > tx->clocks[node(ptr->ts)]) {
		timestamp_share(node(ptr->ts), ptr->ts);
		if (!rwset_check(tx)) trc_abort_ea(tx);
		tx->clocks[node(ptr->ts)] = ptr->ts;
	}

	/* Well done */
	return ptr->content[offset];
}

word_t trc_read_la(tx_t* tx, word_t addr) {
	op_t* ptr;
	byte_t offset;

	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	ptr = rwset_search(tx, addr);

	/* Found on previous read/write record */
	if (ptr->addr) {
		return ptr->content[offset];
	}

	/* Not found. Trying to read it now */
	ptr->addr = addr;
	ptr->ts = mem_read(addr, ptr->content);
	ptr->op = READ_OP;

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_la(tx);
	}

	/* Do we need to extend? */
	if (ptr->ts > tx->clocks[node(ptr->ts)]) {
		timestamp_share(node(ptr->ts), ptr->ts);
		if (!rwset_check_all(tx)) trc_abort_la(tx);
		tx->clocks[node(ptr->ts)] = ptr->ts;
	}

	/* Well done */
	return ptr->content[offset];
}

word_t trc_read(word_t addr) {
	tx_t* tx;
	tx = rwset_locate();
	switch(tx->mode) {
	case LA: return trc_read_la(tx, addr);
	case EA: return trc_read_ea(tx, addr);
	case AA: return trc_read_aa(tx, addr);
	default: printf("%d unknown mode\n", mpi_rank); abort();
	}
}

void trc_write_aa(tx_t* tx, word_t addr, word_t content) {
	op_t* ptr;
	byte_t offset;
	
	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	// debug("%d: write %ld\n", mpi_rank, addr);
	ptr = rwset_search(tx, addr);

	/* Found on previous write record */
	if (ptr->addr && ptr->op == WRITE_OP) {
		ptr->content[offset] = content;
		return;
	}

	/* Found on previous read record? Turn it to write */
	if (ptr->addr && ptr->op == READ_OP) {
		ptr->op = WRITE_OP;
		memcpy(ptr->backup, ptr->content, sizeof(cache_t));
		tx->read_only = 0;
		ptr->content[offset] = content;
		return;
	}

	/* Not found. Try to lock it now */
	ptr->op = WRITE_OP;
	ptr->addr = addr;
	ptr->ts = mem_rdwithdraw(addr, ptr->content);
	memcpy(ptr->backup, ptr->content, sizeof(cache_t));

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_aa(tx);
	}

	/* Well done */
	tx->read_only = 0;
	ptr->content[offset] = content;
}

void trc_write_ea(tx_t* tx, word_t addr, word_t content) {
	op_t* ptr;
	byte_t offset;

	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	ptr = rwset_search(tx, addr);

	/* Found previous write record. Excellent */
	if (ptr->addr && ptr->op == WRITE_OP) {
		ptr->content[offset] = content;
		return;
	}

	/* Found on previous read record? Turn it to write */
	if (ptr->addr && ptr->op == READ_OP) {
		ptr->ts2 = mem_withdraw(addr, ptr->ts);
		if (ptr->ts2 != ptr->ts) trc_abort_ea(tx);
		ptr->op = WRITE_OP;
		memcpy(ptr->backup, ptr->content, sizeof(cache_t));
		tx->read_only = 0;
		ptr->content[offset] = content;
		return;
	}

	/* Not found. Do it now */
	ptr->op = WRITE_OP;
	ptr->addr = addr;
	ptr->ts = mem_rdwithdraw(addr, ptr->content);
	memcpy(ptr->backup, ptr->content, sizeof(cache_t));

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_ea(tx);
	}

	/* Do we need to extend? */
	if (ptr->ts > tx->clocks[node(ptr->ts)]) {
		timestamp_share(node(ptr->ts), ptr->ts);
		if (!rwset_check(tx)) trc_abort_ea(tx);
		tx->clocks[node(ptr->ts)] = ptr->ts;
	}

	/* Well done */
	tx->read_only = 0;
	ptr->content[offset] = content;
}

void trc_write_la(tx_t* tx, word_t addr, word_t content) {
	op_t* ptr;
	byte_t offset;

	offset = (addr / sizeof(word_t)) % line_size;
	addr = base(addr);
	ptr = rwset_search(tx, addr);

	/* Found previous write record. Excellent */
	if (ptr->addr && ptr->op == WRITE_OP) {
		ptr->content[offset] = content;
		return;
	}

	/* Found on previous read record? Turn it to write */
	if (ptr->addr && ptr->op == READ_OP) {
		ptr->op = WRITE_OP;
		memcpy(ptr->backup, ptr->content, sizeof(cache_t));
		tx->read_only = 0;
		ptr->content[offset] = content;
		return;
	}

	/* Not found. Trying to read it now */
	ptr->op = WRITE_OP;
	ptr->addr = addr;
	ptr->ts = mem_read(addr, ptr->content);
	memcpy(ptr->backup, ptr->content, sizeof(cache_t));

	/* Is the read valid? */
	if (ptr->ts == max_word) {
		rwset_pop(tx);
		trc_abort_la(tx);
	}

	/* Do we need to extend? */
	if (ptr->ts > tx->clocks[node(ptr->ts)]) {
		timestamp_share(node(ptr->ts), ptr->ts);
		if (!rwset_check_all(tx)) trc_abort_la(tx);
		tx->clocks[node(ptr->ts)] = ptr->ts;
	}

	/* Well done */
	tx->read_only = 0;
	ptr->content[offset] = content;
}

void trc_write(word_t addr, word_t content) {
	tx_t* tx;
	tx = rwset_locate();
	switch(tx->mode) {
	case LA: return trc_write_la(tx, addr, content);
	case EA: return trc_write_ea(tx, addr, content);
	case AA: return trc_write_aa(tx, addr, content);
	default: printf("%d unknown mode\n", mpi_rank); abort();
	}
}

static void read_write_test1(void) {
	roll_t roll;
	hint_t hint;
	word_t i;
	word_t x;
	word_t y;

	for (i = 4120; i < mem_count * sizeof(cache_t);
	     i += 435677 * sizeof(cache_t))
	{
		sigsetjmp(roll, 0);
		trc_begin(&roll, &hint);

		x = trc_read(i+sizeof(word_t));
		x += 2;
		trc_write(i+sizeof(word_t), x);

		x = trc_read(i+sizeof(word_t)*2388);
		x += 4;
		trc_write(i+sizeof(word_t)*2388, x);

		x = trc_read(i);
		x += 1;
		trc_write(i, x);

		y = trc_read(i);
		if (x != y) debug("%d: unwritable %ld: %ld\n", mpi_rank, i, x);
		trc_commit();
	}
	sleep(10);
}

static void read_write_test2(void) {
	roll_t roll;
	hint_t hint;
	word_t i;
	word_t x;

	for (i = 4120; i < mem_count * sizeof(cache_t);
	     i += 435677 * sizeof(cache_t))
	{
		sigsetjmp(roll, 0);
		trc_begin(&roll, &hint);
		x = trc_read(i);
		trc_commit();
		if (x != mpi_size) debug("%d: inconsistent %ld: %ld\n",
		                         mpi_rank, i, x);
		trc_begin(&roll, &hint);
		x = trc_read(i+sizeof(word_t));
		trc_commit();
		if (x != mpi_size * 2) debug("%d: inconsistent(2) %ld: %ld\n",
		                              mpi_rank, i, x);
		trc_begin(&roll, &hint);
		x = trc_read(i+sizeof(word_t)*2388);
		trc_commit();
		if (x != mpi_size * 4) debug("%d: inconsistent(4) %ld: %ld\n",
		                              mpi_rank, i, x);
	}
}

void trc_read_write_test(void) {
	info("%d: testing read / write consistency...\n", mpi_rank);
	read_write_test1();
	sleep(20);
	info("%d: double checking read / write consistency...\n", mpi_rank);
	read_write_test2();
}

static void read_write_init(void) {

}

static void read_write_exit(void) {

}

