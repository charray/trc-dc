/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static word_t alloc_start;
static word_t alloc_end;
static volatile word_t alloc_buddies[64];

static word_t alloc_withdraw_or_fail(word_t addr, word_t* content) {
	word_t ts;
	ts = mem_rdwithdraw(addr, content);
	if (ts == max_word) {
		printf("%d: unable to withdraw %ld for alloc linked "
		       "list!\n", mpi_rank, addr);
		abort();
	}
	return ts;
}

static word_t alloc_read_or_fail(word_t addr, word_t* content) {
	word_t ts;
	ts = mem_read(addr, content);
	if (ts == max_word) {
		printf("%d: unable to read %ld for alloc linked "
		       "list!\n", mpi_rank, addr);
		abort();
	}
	return ts;
}

static word_t alloc_hash(byte_t size) {
	return (size << 16) + ((size * 3) << 8) + size * 2;
}

static byte_t alloc_dehash(word_t hash) {
	byte_t a, b, c;
	a = (hash & 255);
	b = ((hash >> 8) & 255);
	c = ((hash >> 16) & 255);
	if (a != c * 2 || b != c * 3) return 0;
	return c;
}

static void alloc_join(word_t addr, word_t addr2) {
	word_t ts;
	byte_t new_size;
	word_t content[line_size];

	ts = alloc_withdraw_or_fail(addr, content);
	new_size = alloc_dehash(content[0]) + 1;
	content[0] = alloc_hash(new_size);
	content[1] = 0;
	mem_deposit(addr, ts, content);
	/* debug("%d: join %ld and %ld into %ld of size %d\n",
	      mpi_rank, addr, addr2, addr, new_size); */
}

static word_t alloc_split(word_t addr) {
	word_t ts;
	word_t content[line_size];
	byte_t new_size;
	
	ts = alloc_withdraw_or_fail(addr, content);
	new_size = alloc_dehash(content[0]) - 1;
	content[0] = alloc_hash(new_size);
	content[1] = 0;
	mem_deposit(addr, ts, content);

	ts = alloc_withdraw_or_fail(addr + ((word_t)1 << new_size), content);
	content[0] = alloc_hash(new_size);
	content[1] = 0;
	mem_deposit(addr + ((word_t)1 << new_size), ts, content);
	/* debug("%d: split %ld into %ld and %ld of size %d\n", mpi_rank, addr,
	      addr, addr + ((word_t)1 << new_size), new_size); */
	return addr + ((word_t)1 << new_size);
}

static byte_t alloc_propose(word_t addr) {
	byte_t answer;
	answer = 1;
	while (((addr >> answer) << answer) == addr
	       && addr + ((word_t)1 << answer) <= alloc_end) ++answer;
	return answer - 1;
}

static byte_t alloc_joinable(word_t addr, word_t addr2, byte_t size) {
	return (addr + ((word_t)1 << size) == addr2 &&
	        alloc_propose(addr) > size);
}

static void alloc_put(word_t addr, byte_t size) {
	word_t first, node, next, ts, ts2;
	word_t content[line_size];
	word_t content2[line_size];

	if (cache_home(addr) != mpi_rank) {
		comm_enqueue(DEALLOC, cache_home(addr), addr, size, 0, 0, 0);
		return;
	}

retry:	node = test(&alloc_buddies[size], max_word);
	if (node == max_word) goto retry;
	/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, node, max_word); */

	/* List empty */
	if (node == 0) {
		ts = alloc_withdraw_or_fail(addr, content);
		content[1] = 0;
		mem_deposit(addr, ts, content);
		alloc_buddies[size] = addr;
		/* debug("%d: list %d %ld->%ld\n", mpi_rank, size,
		      max_word, addr); */
		/* debug("%d: insert as unique, %ld into list %d\n",
		      mpi_rank, addr, size); */
		return;
	}

	/* To be inserted at begin, but indeed joinable */
	if (addr < node && alloc_joinable(addr, node, size)) {
		ts = alloc_withdraw_or_fail(node, content);
		alloc_buddies[size] = content[1];
		/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, max_word,
		      content[1]); */
		mem_deposit(node, ts, content);
		alloc_join(addr, node);
		alloc_put(addr, size + 1);
		return;
	}

	/* To be inserted at begin */
	if (addr < node) {
		ts = alloc_withdraw_or_fail(addr, content);
		content[1] = node;
		mem_deposit(addr, ts, content);
		alloc_buddies[size] = addr;
		/* debug("%d: list %d %ld->%ld\n", mpi_rank, size,
		      max_word, addr); */
		/* debug("%d: inserted as first, %ld into list %d, "
		      "next: %ld\n", mpi_rank, addr, size, node); */
		return;
	}
	
	/* To be inserted right after begin, but indeed joinable */
	if (alloc_joinable(node, addr, size)) {
		ts = alloc_read_or_fail(node, content);
		alloc_buddies[size] = content[1];
		/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, max_word,
		      content[1]); */
		alloc_join(node, addr);
		alloc_put(node, size + 1);
		return;
	}

	/* There are at least one node here */
	first = node;
	while (1) {
		ts = alloc_withdraw_or_fail(node, content);
		next = content[1];

		/* Is it joinable with the next? */
		if (alloc_joinable(addr, next, size)) {
			ts2 = alloc_read_or_fail(next, content2);
			content[1] = content2[1];
			mem_deposit(node, ts, content);
			alloc_join(addr, next);
			alloc_put(addr, size + 1);
			break;
		}

		/* Join at another direction? */
		if (alloc_joinable(next, addr, size)) {
			ts2 = alloc_read_or_fail(next, content2);
			content[1] = content2[1];
			mem_deposit(node, ts, content);
			alloc_join(next, addr);
			alloc_put(next, size + 1);
			break;
		}

		/* Not joinable, but insertable? */
		if ((node < addr && addr < next) || next == 0) {
			ts2 = alloc_withdraw_or_fail(addr, content2);
			content2[1] = next;
			mem_deposit(addr, ts2, content2);
			content[1] = addr;
			mem_deposit(node, ts, content);
			/* debug("%d: inserted in middle, %ld into list %d, "
			      "prev: %ld, next: %ld\n",
			      mpi_rank, addr, size, node, next); */
			break;
		}
		mem_deposit(node, ts, content);
		node = next;
	}

	/* Release the locked list */
	alloc_buddies[size] = first;
	/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, max_word, first); */
}

static void alloc_put_fast(word_t addr, byte_t size) {
	word_t first, node, next, ts, ts2;
	word_t content[line_size];
	word_t content2[line_size];

retry:	node = test(&alloc_buddies[size], max_word);
	if (node == max_word) goto retry;
	/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, node, max_word); */

	/* Is it the first on the updated list? */
	if (node == 0 || addr < node) {
		ts = alloc_withdraw_or_fail(addr, content);
		content[1] = node;
		mem_deposit(addr, ts, content);
		alloc_buddies[size] = addr;
		/* debug("%d: list %d %ld->%ld\n", mpi_rank, size,
		      max_word, addr); */
		/* debug("%d: fast insert %ld into list %d, next: %ld\n",
		      mpi_rank, addr, size, node); */
		return;
	}

	/* There are at least one node here */
	first = node;
	while (1) {
		ts = alloc_withdraw_or_fail(node, content);
		next = content[1];
		if ((node < addr && addr < next) || next == 0) {
			ts2 = alloc_withdraw_or_fail(addr, content2);
			content2[1] = next;
			mem_deposit(addr, ts2, content2);

			content[1] = addr;
			mem_deposit(node, ts, content);
			/* debug("%d: fast insert %ld into list %d, "
			      "prev: %ld, next: %ld\n",
			      mpi_rank, addr, size, node, next); */
			break;
		}
		mem_deposit(node, ts, content);
		node = next;
	}

	/* Release the locked list */
	alloc_buddies[size] = first;
	/* debug("%d: list %d %ld->%ld\n", mpi_rank, size, max_word, first); */
}

static word_t alloc_get_impl(byte_t size, byte_t size2) {
	word_t candidate, spare, ts;
	word_t content[line_size];
	volatile word_t candidate2;

	if (size >= 64) return 0;
	while (((word_t)1 << size) < sizeof(cache_t)) size++;
retry:	candidate = test(&alloc_buddies[size], max_word);
	if (candidate == max_word) goto retry;

	/* If nothing is in this list, get a upper one */
	if (candidate == 0) {
		alloc_buddies[size] = 0;
		candidate = alloc_get_impl(size + 1, size2);
		if (candidate == 0) goto nomem;
		spare = alloc_split(candidate);
		alloc_put_fast(spare, size);
	/* Otherwise, just remove the first one from list */
	} else {
		ts = alloc_read_or_fail(candidate, content);
		alloc_buddies[size] = content[1];
	}

	return candidate;

nomem:	if (size != size2) return 0;
	candidate2 = max_word - 1;
	comm_enqueue(MALLOC, mpi_next, size, 0, 0, &candidate2, 0);
	while (candidate2 == max_word - 1);
	if (!candidate2) info("%d: run out of memory\n", mpi_rank);
	return candidate2;
}

static word_t alloc_get(byte_t size) {
	while (((word_t)1 << size) < sizeof(cache_t)) size++;
	return alloc_get_impl(size, size);
}

static void* alloc_test_thread(void* args) {
	word_t ptr, ptr2, ptr3, ptr4;
	int i;

	for (i = 0; i < 1000; ++i) {
		ptr = alloc_get(24);
		alloc_put(ptr, 24);
	
		ptr2 = alloc_get(26);
		ptr3 = alloc_get(27);
		ptr4 = alloc_get(26);
	
		alloc_put(ptr2, 26);
		ptr2 = alloc_get(25);
		alloc_put(ptr2, 25);
		alloc_put(ptr3, 27);
		alloc_put(ptr4, 26);
	}
	
	return 0;
}

static void alloc_test(void) {
	pthread_t threads[16];
	void* dummy;
	int i;

	for (i = 0; i < 16; ++i) {
		pthread_create(threads+i, 0, alloc_test_thread, 0);
	}

	for (i = 0; i < 16; ++i) {
		pthread_join(threads[i], &dummy);
	}
}

static void alloc_test2(void) {
	byte_t i;
	word_t ptr;
	for (i = 0; i < 64; ++i) {
		if (!alloc_buddies[i]) continue;
		debug("%d: %d: %ld\n", mpi_rank, i, alloc_buddies[i]);
	}
	if (!mpi_rank) debug("%d: try something strange...\n", mpi_rank);
	if (mpi_rank == 0) ptr = alloc_get(31);
	if (!mpi_rank) debug("%d: %ld (%d)\n", mpi_rank, ptr, cache_home(ptr));
	if (!mpi_rank) alloc_put(ptr, 31);
	if (mpi_rank == 0) ptr = alloc_get(31);
	if (!mpi_rank) debug("%d: %ld (%d)\n", mpi_rank, ptr, cache_home(ptr));
	if (mpi_rank == 0) ptr = alloc_get(31);
	if (!mpi_rank) debug("%d: %ld (%d)\n", mpi_rank, ptr, cache_home(ptr));
	if (mpi_rank == 0) ptr = alloc_get(31);
	if (!mpi_rank) debug("%d: %ld (%d)\n", mpi_rank, ptr, cache_home(ptr));
}

void trc_alloc_test(void) {
	alloc_test();
	alloc_test2();
}

static void alloc_init(void) {
	word_t ptr, ts;
	word_t content[line_size];
	byte_t size;

	alloc_start = cache_start;
	alloc_end = cache_end;
	memset((void*)alloc_buddies, 0, sizeof(alloc_buddies));

	for (ptr = alloc_start; ptr < alloc_end; ptr += ((word_t)1 << size)) {
		size = alloc_propose(ptr);
		ts = alloc_withdraw_or_fail(ptr, content);
		content[0] = alloc_hash(size);
		mem_deposit(ptr, ts, content);
		alloc_put_fast(ptr, size);
	}

	info("%d: allocation unit initialised\n", mpi_rank);
}

static void alloc_exit(void) {

}

