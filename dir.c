/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define INVALID 0
#define SHARED 2
#define EXCLUSIVE 4
#define WITHDRAWN 6
#define OVERFLOW_SHARED 10
#define OVERFLOW_EXCLUSIVE 12
#define UPDATING 8

#define num(x)          (x / sizeof(cache_t))

static volatile word_t* dir;

typedef union {
	word_t content;
	struct {
		byte_t status;
		byte_t neighbours[7];
	} actual;
} dir_t;

typedef word_t
	cache_t[line_size];

typedef struct {
	word_t addr;
	word_t ts;
} meta_t;

/* Status functions */
static byte_t dir_get(word_t addr) {
	dir_t temp;
	temp.content = dir[num(addr)];
	return temp.actual.status;
}

static byte_t dir_cas(word_t addr, byte_t old, byte_t new) {
	dir_t temp, temp2;
retry:	temp.content = temp2.content = dir[num(addr)];
	if (temp.actual.status != old) return 0;
	temp2.actual.status = new;
	if (cas(&dir[num(addr)], temp.content, temp2.content)) return 1;
	goto retry;
}

static void dir_set(word_t addr, byte_t new) {
	dir_t temp;
	temp.content = dir[num(addr)];
	temp.actual.status = new;
	dir[num(addr)] = temp.content;
}

/* Neighbour list functions */
static byte_t dir_add(word_t addr, byte_t peer) {
	int i;
	dir_t temp;
	temp.content = dir[num(addr)];
	for (i = 0; i < 7; ++i) {
		if (temp.actual.neighbours[i] != max_byte) continue;
		temp.actual.neighbours[i] = peer;
		dir[num(addr)] = temp.content;
		return 1;
	}
	return 0;
}

static void dir_list(word_t addr, byte_t* peers) {
	int i;
	dir_t temp;
	temp.content = dir[num(addr)];
	for (i = 0; i < 7; ++i) peers[i] = temp.actual.neighbours[i];
}

static byte_t dir_first(word_t addr) {
	dir_t temp;
	temp.content = dir[num(addr)];
	return temp.actual.neighbours[0];
}

static void dir_clear(word_t addr) {
	int i;
	dir_t temp;
	temp.content = dir[num(addr)];
	for (i = 0; i < 7; ++i) temp.actual.neighbours[i] = max_byte;
	dir[num(addr)] = temp.content;
}

/* Init and exit functions */
static void dir_init(void) {
	dir = malloc(sizeof(word_t) * mem_count);
	info("%d: directory initialised\n", mpi_rank);
}

static void dir_exit(void) {
	free((void*)dir);
}

