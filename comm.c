/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static pthread_t comm_thread;

typedef struct {
	byte_t  action;
	byte_t  serv;
	byte_t  source;
	word_t  addr;
	word_t  ts;
	word_t  content[line_size];
	volatile word_t* ts_ptr;
	word_t* content_ptr;
#ifdef migration
	volatile word_t* parent_ptr;
#endif
} queue_t;

static byte_t  comm_continue;
static queue_t comm_queue[cq_length];
static volatile word_t cq_head;
static volatile word_t cq_tail;
static volatile word_t cq_mutex;

#define TS          2
#define READ        4
#define FWREAD      6
#define WITHDRAW    8
#define RDWITHDRAW 10
#define ABANDON    12
#define DEPOSIT    14
#define RETURN     16
#define CACHE      18
#define MALLOC     32 
#define DEALLOC    34
#define STOP       64
#define BARRIER    66

#define cas(x,y,z)      __sync_bool_compare_and_swap(x,y,z)
#define inc(x,y)        __sync_fetch_and_add(x,y)
#define test(x,y)       __sync_lock_test_and_set(x,y)
#define msg_max         (cq_length * (line_size + 6))
#define release(x)      __sync_lock_release(x)

#ifdef migration
static void comm_forward_with_parent(byte_t action, byte_t serv, byte_t source, word_t addr, word_t ts, word_t* content, volatile word_t* ts_ptr, word_t* content_ptr, volatile word_t* parent_ptr) {
	word_t tail;

	/* Wait for the mutex to enqueue */
	while (test(&cq_mutex, 1));

	/* When the queue is full, do not proceed */
	/* The consumer does not need mutex above to dequeue */
	while ((cq_head + cq_length - 1) % cq_length == cq_tail);
	tail = cq_tail;

	/* Copy the scalar information */
	comm_queue[tail].action = action;
	comm_queue[tail].serv = serv;
	comm_queue[tail].source = source;
	comm_queue[tail].addr = addr;
	comm_queue[tail].ts = ts;
	comm_queue[tail].ts_ptr = ts_ptr;
	comm_queue[tail].content_ptr = content_ptr;
	comm_queue[tail].parent_ptr = parent_ptr;
 
 	/* Copy the memory content, if given */
	if (content) {
		memcpy(comm_queue[tail].content,
		       content, sizeof(word_t) * line_size);
	}

	/* Set the position to the next one */
	cq_tail = (tail + 1) % cq_length;
	release(&cq_mutex);
}
#endif

static void comm_forward(byte_t action, byte_t serv, byte_t source, word_t addr, word_t ts, word_t* content, volatile word_t* ts_ptr, word_t* content_ptr) {
	word_t tail;

	/* Wait for the mutex to enqueue */
	while (test(&cq_mutex, 1));

	/* When the queue is full, do not proceed */
	/* The consumer does not need mutex above to dequeue */
	while ((cq_head + cq_length - 1) % cq_length == cq_tail);
	tail = cq_tail;

	/* Copy the scalar information */
	comm_queue[tail].action = action;
	comm_queue[tail].serv = serv;
	comm_queue[tail].source = source;
	comm_queue[tail].addr = addr;
	comm_queue[tail].ts = ts;
	comm_queue[tail].ts_ptr = ts_ptr;
	comm_queue[tail].content_ptr = content_ptr;
#ifdef migration
	comm_queue[tail].parent_ptr = 0;
#endif
 
 	/* Copy the memory content, if given */
	if (content) {
		memcpy(comm_queue[tail].content,
		       content, sizeof(word_t) * line_size);
	}

	/* Set the position to the next one */
	cq_tail = (tail + 1) % cq_length;
	release(&cq_mutex);
}

#ifdef migration
static void comm_enqueue_with_parent(byte_t action, byte_t serv, word_t addr, word_t ts, word_t* content, volatile word_t* ts_ptr, word_t* content_ptr, volatile word_t* parent_ptr) {
	comm_forward_with_parent(action, serv, mpi_rank, addr, ts, content,
	                         ts_ptr, content_ptr, parent_ptr);
}
#endif

static void comm_enqueue(byte_t action, byte_t serv, word_t addr, word_t ts, word_t* content, volatile word_t* ts_ptr, word_t* content_ptr) {
	comm_forward(action, serv, mpi_rank, addr, ts, content,
	             ts_ptr, content_ptr);
}

static word_t comm_compress(queue_t* req, word_t* msg, word_t offset) {
	byte_t i;

	msg[offset++] = (req->source << 16) + req->action;
	msg[offset++] = req->addr;

	switch(req->action) {
	case TS:
		msg[offset++] = (word_t)req->ts_ptr;
#ifdef migration
		msg[offset++] = (word_t)req->parent_ptr;
#endif
		break;
	case READ:
		msg[offset++] = (word_t)req->ts_ptr;
		msg[offset++] = (word_t)req->content_ptr;
#ifdef migration
		msg[offset++] = (word_t)req->parent_ptr;
#endif
		break;
	case FWREAD:
		msg[offset++] = (word_t)req->ts_ptr;
		msg[offset++] = (word_t)req->content_ptr;
		break;
	case WITHDRAW:
		msg[offset++] = req->ts;
		msg[offset++] = (word_t)req->ts_ptr;
#ifdef migration
		msg[offset++] = (word_t)req->parent_ptr;
#endif
		break;
	case RDWITHDRAW:
		msg[offset++] = (word_t)req->ts_ptr;
		msg[offset++] = (word_t)req->content_ptr;
#ifdef migration
		msg[offset++] = (word_t)req->parent_ptr;
#endif
		break;
	case ABANDON:
		msg[offset++] = (word_t)req->ts_ptr;
		break;
	case DEPOSIT:
		msg[offset++] = req->ts;
		msg[offset++] = (word_t)req->ts_ptr;
		for (i = 0; i < line_size; ++i) msg[offset++] = req->content[i];
		break;
	case RETURN:
		msg[offset++] = (word_t)req->ts_ptr;
		msg[offset++] = req->ts;
		break;
	case CACHE:
		msg[offset++] = (word_t)req->ts_ptr;
		msg[offset++] = req->ts;
		msg[offset++] = (word_t)req->content_ptr;
		for (i = 0; i < line_size; ++i) msg[offset++] = req->content[i];
		break;
	case MALLOC:
		msg[offset++] = (word_t)req->ts_ptr;
		break;
	case DEALLOC:
		msg[offset++] = req->ts;
		break;
	default:
		break;
	}
	return offset;
}

static word_t comm_extract(queue_t* req, word_t* msg, word_t offset) {
	byte_t i;

	req->serv = msg[offset] >> 16;
	req->action = (msg[offset++] & max_word);
	req->addr = msg[offset++];

	switch(req->action) {
	case TS:
		req->ts_ptr = (word_t*)msg[offset++];
#ifdef migration
		req->parent_ptr = (word_t*)msg[offset++];
#endif
		break;
	case READ:
		req->ts_ptr = (word_t*)msg[offset++];
		req->content_ptr = (word_t*)msg[offset++];
#ifdef migration
		req->parent_ptr = (word_t*)msg[offset++];
#endif
		break;
	case FWREAD:
		req->ts_ptr = (word_t*)msg[offset++];
		req->content_ptr = (word_t*)msg[offset++];
		break;
	case WITHDRAW:
		req->ts = msg[offset++];
		req->ts_ptr = (word_t*)msg[offset++];
#ifdef migration
		req->parent_ptr = (word_t*)msg[offset++];
#endif
		break;
	case RDWITHDRAW:
		req->ts_ptr = (word_t*)msg[offset++];
		req->content_ptr = (word_t*)msg[offset++];
#ifdef migration
		req->parent_ptr = (word_t*)msg[offset++];
#endif
		break;
	case ABANDON:
		req->ts_ptr = (word_t*)msg[offset++];
		break;
	case DEPOSIT:
		req->ts = msg[offset++];
		req->ts_ptr = (word_t*)msg[offset++];
		for (i = 0; i < line_size; ++i) req->content[i] = msg[offset++];
		break;
	case RETURN:
		req->ts_ptr = (word_t*)msg[offset++];
		req->ts = msg[offset++];
		break;
	case CACHE:
		req->ts_ptr = (word_t*)msg[offset++];
		req->ts = msg[offset++];
		req->content_ptr = (word_t*)msg[offset++];
		for (i = 0; i < line_size; ++i) req->content[i] = msg[offset++];
		break;
	case MALLOC:
		req->ts_ptr = (word_t*)msg[offset++];
		break;
	case DEALLOC:
		req->ts = msg[offset++];
		break;
	default:
		break;
	}

	return offset;
}

static void* comm_test1_thread1(void* args) {
	word_t* dummy;
	word_t* dummy2;
	word_t* dummy3;
	dummy = malloc(sizeof(word_t) * line_size);
	dummy2 = malloc(sizeof(word_t));
	dummy3 = malloc(sizeof(word_t) * line_size);
	memset(dummy, 0, sizeof(word_t) * line_size);
	memset(dummy2, 0, sizeof(word_t));
	memset(dummy3, 0, sizeof(word_t) * line_size);
	dummy[0] = 1;
	dummy[1] = 2;
	dummy[2] = 3;
	dummy[3] = 4;
	dummy[4] = 5;
	dummy[5] = 6;
	dummy[6] = 7;
	dummy[7] = 8;
	dummy3[0] = 1;
	dummy3[1] = 2;
	dummy3[2] = 3;
	dummy3[3] = 4;
	dummy3[4] = 5;
	dummy3[5] = 6;
	dummy3[6] = 7;
	dummy3[7] = 8;
	comm_enqueue(TS, 1, 1024, 0, 0, dummy2, 0);
	comm_enqueue(READ, 2, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(FWREAD, 3, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(WITHDRAW, 1, 1024, 32, 0, dummy2, 0);
	comm_enqueue(RDWITHDRAW, 1, 1024, 32, 0, dummy2, dummy);
	comm_enqueue(ABANDON, 1, 1024, 64, 0, 0, 0);
	comm_enqueue(DEPOSIT, 1, 2048, 32, dummy3, 0, 0);
	comm_enqueue(RETURN, 4, 8192, 64, 0, dummy2, 0);
	comm_enqueue(CACHE, 3, 8192, 64, dummy3, dummy2, dummy);
	comm_enqueue(TS, 1, 1024, 0, 0, dummy2, 0);
	comm_enqueue(READ, 2, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(FWREAD, 3, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(WITHDRAW, 1, 1024, 32, 0, dummy2, 0);
	comm_enqueue(RDWITHDRAW, 1, 1024, 32, 0, dummy2, dummy);
	comm_enqueue(ABANDON, 1, 1024, 64, 0, 0, 0);
	comm_enqueue(DEPOSIT, 1, 2048, 32, dummy3, 0, 0);
	comm_enqueue(RETURN, 4, 8192, 64, 0, dummy2, 0);
	comm_enqueue(CACHE, 3, 8192, 64,dummy3, dummy2, dummy);
	comm_enqueue(TS, 1, 1024, 0, 0, dummy2, 0);
	comm_enqueue(READ, 2, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(FWREAD, 3, 2048, 0, 0, dummy2, dummy);
	comm_enqueue(WITHDRAW, 1, 1024, 32, 0, dummy2, 0);
	comm_enqueue(RDWITHDRAW, 1, 1024, 32, 0, dummy2, dummy);
	comm_enqueue(ABANDON, 1, 1024, 64, 0, 0, 0);
	comm_enqueue(DEPOSIT, 1, 2048, 32, dummy3, 0, 0);
	comm_enqueue(RETURN, 4, 8192, 64, 0, dummy2, 0);
	comm_enqueue(CACHE, 3, 8192, 64,dummy3, dummy2, dummy);
	return 0;
}

static void comm_test1(void) {
	pthread_t thread1, thread1x;
	void* dummy;

	pthread_create(&thread1, 0, comm_test1_thread1, 0);
	pthread_create(&thread1x, 0, comm_test1_thread1, 0);
	pthread_join(thread1, &dummy);
	pthread_join(thread1x, &dummy);
}

static void* comm_function(void* args) {
	word_t head, tail, offset, i;
	word_t msg[msg_max];
	byte_t peer;

	while (comm_continue || cq_tail != cq_head) {
		tail = cq_tail;
		head = cq_head;
		if (head == tail) {
			yield();
			continue;
		}

		/* For each node, find the relevent messages, pack and send */
		for (peer = 0; peer < mpi_size; ++peer) {
			offset = 0;
			for (i = head; i != tail; i = (i + 1) % cq_length) {
				if (comm_queue[i].serv != peer) continue;
				offset = comm_compress(comm_queue+i,msg,offset);
			}

			if (!offset) continue;
			msg[offset++] = 0;
			MPI_Send(msg, sizeof(word_t) * offset, MPI_BYTE,
			         peer, 0, mpi_world);
		}

		/* Finished. Mark the tail position as new head */
		cq_head = tail;
		yield();
	}
	return 0;
}

void trc_comm_test(void) {
	info("%d: communication self test...\n", mpi_rank);
	if (mpi_rank == 0) comm_test1();
	info("%d: communication testing done\n", mpi_rank);
}

static void comm_init(void) {
	comm_continue = 1;
	pthread_create(&comm_thread, 0, comm_function, 0);
	info("%d: communication initialised\n", mpi_rank);
}

static void comm_exit(void) {
	void* dummy;
	comm_continue = 0;
	pthread_join(comm_thread, &dummy);
}

