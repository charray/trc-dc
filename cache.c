/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static word_t cache_start;
static word_t cache_end;
static word_t cache_neighbour[max_byte];
static volatile cache_t* cache;
static volatile meta_t* meta;

/* cache .addr convention
   addr + incarn: address + incarn
   max_word_bit + incarn: empty
   max_word: locked

   The incarn number is to ensure consistency.
   It has to be changed everytime a cache slot is modified.
   We have space for storing incarnation number as we do not need to store
   all the bits of an address as the cache placement has certain rules.
   For an address to appear in a slot, we already know its last few bits.
*/

#define base(x)         (x / sizeof(cache_t) * sizeof(cache_t))
#define base2(x)        (x / max_incarn * max_incarn)
#define incarn(x)       (x % max_incarn)
#define search_start(x) (num(x) % (line_count / cache_assoc) * cache_assoc)
#define search_end(x)   (search_start(x) + cache_assoc)
#define max_incarn      (line_count / cache_assoc * sizeof(cache_t))

static byte_t cache_home_impl(word_t addr, byte_t start, byte_t end) {
	byte_t mid;
	mid = (start + end) / 2;

	/* Binary search ... */
	if (start + 1 == end)
		return start;
	if (cache_neighbour[mid] > addr)
		return cache_home_impl(addr, start, mid);
	if (cache_neighbour[mid + 1] <= addr)
		return cache_home_impl(addr, mid + 1, end);
	return mid;
}

static byte_t cache_home(word_t addr) {
	return cache_home_impl(addr, 0, mpi_size);
}

/* Find and lock functions */
static word_t cache_find(word_t addr, word_t* incarn) {
	word_t pos, base_address, meta_content, meta_address, meta_incarn;
	
	base_address = base2(addr);
	for (pos = search_start(addr); pos < search_end(addr); ++pos) {
		meta_content = meta[pos].addr;
		meta_address = base2(meta_content);
		meta_incarn = incarn(meta_content);
		if (base_address != meta_address) continue;
		*incarn = meta_incarn;
		return pos;
	}

	return line_count;
}

static word_t cache_lock(word_t addr, word_t* incarn) {
	word_t pos, base_address, meta_content, meta_address, meta_incarn;

	base_address = base2(addr);
retry:	for (pos = search_start(addr); pos < search_end(addr); ++pos) {
		meta_content = meta[pos].addr;
		meta_address = base2(meta_content);
		meta_incarn = incarn(meta_content);
		if (base_address != meta_address) continue;
		if (!cas(&meta[pos].addr, meta_content, max_word)) goto retry;
		*incarn = meta_incarn;
		return pos;
	}

	return line_count;
}

static word_t cache_lock_free(word_t addr, word_t* incarn) {
	word_t pos, meta_content, meta_address, meta_incarn;

retry:	for (pos = search_start(addr); pos < search_end(addr); ++pos) {
		meta_content = meta[pos].addr;
		meta_address = base2(meta_content);
		meta_incarn = incarn(meta_content);
		if (meta_address != max_word_bit) continue;
		if (!cas(&meta[pos].addr, meta_content, max_word)) goto retry;
		*incarn = meta_incarn;
		return pos;
	}

	return line_count;
}

/* Verify and release functions */
static byte_t cache_verify(word_t pos, word_t addr, word_t incarn) {
	return (meta[pos].addr == base2(addr) + incarn);
}

static void cache_release_fast(word_t pos, word_t addr, word_t incarn) {
	meta[pos].addr = base2(addr) + incarn;
}

static void cache_release_clean(word_t pos, word_t incarn) {
	meta[pos].addr = max_word_bit + incarn;
	meta[pos].ts = 0;
	memset((void*)cache[pos], 0, sizeof(cache_t));
}


static void cache_release(word_t pos, word_t addr, word_t ts, const word_t* content, word_t incarn) {
	incarn = (incarn + 1) % max_incarn;
	memcpy((void*)cache[pos], content, sizeof(cache_t));
	meta[pos].ts = ts;
	meta[pos].addr = base2(addr) + incarn;
}

/* Interface for getting and putting cache content */
static word_t cache_read(word_t addr, word_t* content) {
	word_t pos, ts, incarn;

retry:	pos = cache_find(addr, &incarn);
	if (pos == line_count) return max_word;
	ts = meta[pos].ts;
	memcpy(content, (void*)cache[pos], sizeof(cache_t));
	if (!cache_verify(pos, addr, incarn)) goto retry;
	return ts;
}

static word_t cache_timestamp(word_t addr) {
	word_t pos, ts, incarn;

retry:	pos = cache_find(addr, &incarn);
	if (pos == line_count) return max_word;
	ts = meta[pos].ts;
	if (!cache_verify(pos, addr, incarn)) goto retry;
	return ts;
}

static word_t cache_withdraw(word_t addr, word_t ts) {
	word_t pos, incarn, result;

	pos = cache_lock(addr, &incarn);
	if (pos == line_count) {
		return max_word;
	}

	result = meta[pos].ts;
	if (result != ts) {
		cache_release_fast(pos, addr, incarn);
		return result;
	}

	if (!dir_cas(addr, EXCLUSIVE, WITHDRAWN)) {
		cache_release_fast(pos, addr, incarn);
		return max_word - 1;
	}

	cache_release_clean(pos, incarn);
	return ts;
}

static word_t cache_rdwithdraw(word_t addr, word_t* content) {
	word_t pos, incarn, result;

	pos = cache_lock(addr, &incarn);
	if (pos == line_count) {
		return max_word;
	}

	if (!dir_cas(addr, EXCLUSIVE, WITHDRAWN)) {
		cache_release_fast(pos, addr, incarn);
		return max_word - 1;
	}

	memcpy(content, (void*)cache[pos], sizeof(cache_t));
	result = meta[pos].ts;
	cache_release_clean(pos, incarn);
	return result;
}

static word_t cache_deposit(word_t addr, word_t ts, const word_t* content) {
	word_t pos, incarn;
	pos = cache_lock_free(addr, &incarn);
	if (pos == line_count) return max_word;

	if (!dir_cas(addr, WITHDRAWN, EXCLUSIVE)) {
		cache_release_clean(pos, incarn);
		return max_word;
	}

	cache_release(pos, addr, ts, content, incarn);
	return ts;
}

/* Self test procedures */
static void cache_test1(void) {
	word_t addr, ts;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
		ts = cache_read(addr, content);
		if (ts == max_word)
			printf("%d: inaccessible %ld (1)\n", mpi_rank, addr);
		else if (ts != content[0])
			printf("%d: inconsistent %ld (1)\n", mpi_rank, addr);
	}
}

static void cache_test2(void) {
	word_t addr, ts;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
		ts = cache_rdwithdraw(addr, content);
		if (ts == max_word) {
			printf("%d: non-withdrawable %ld (2)\n", mpi_rank,addr);
			continue;
		}

		ts = addr;
		content[0] = addr;
		content[line_size - 1] = addr * addr;
		if (cache_deposit(addr, ts, content) == max_word) {
			printf("%d: non-depositable %ld (2)\n", mpi_rank, addr);
			continue;
		}
	}
}

static void cache_test3(void) {
	word_t addr, ts;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
		ts = cache_read(addr, content);
		if (ts == max_word)
			printf("%d: inaccessible %ld (3)\n", mpi_rank, addr);
		else if (ts != content[0] || addr != content[0])
			printf("%d: inconsistent %ld (3)\n", mpi_rank, addr);
		else if (addr * addr != content[line_size - 1])
			printf("%d: inconsistent %ld (3)\n", mpi_rank, addr);
	}
}

static void cache_test4(void) {
	word_t addr, ts;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
		ts = cache_rdwithdraw(addr, content);
		if (ts == max_word) {
			printf("%d: non-withdrawable %ld (4)\n", mpi_rank,addr);
			continue;
		}
		ts = content[0] = content[line_size - 1] = 0;
		if (cache_deposit(addr, ts, content) == max_word) {
			printf("%d: non-depositable %ld (4)\n", mpi_rank, addr);
			continue;
		}
	}
}

static void* cache_test5_thread(void* args) {
	word_t addr, ts;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
retry:		ts = cache_rdwithdraw(addr, content);
		if (ts == max_word) goto retry;
		ts = content[0] = content[line_size - 1] = ts + 1;
		cache_deposit(addr, ts, content);
	}
	return 0;
}

static void* cache_test5_thread2(void* args) {
	word_t addr, ts;
	cache_t content;
	
	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
retry:		ts = cache_read(addr, content);
		if (ts == max_word) goto retry;
		if (ts != content[0] || ts != content[line_size - 1])
			printf("%d: inconsistent %ld (5): %ld %ld %ld\n",
			       mpi_rank, addr, ts, content[0],
			       content[line_size - 1]);
	}
	return 0;
}

static void cache_test5(void) {
	pthread_t thread1, thread2, thread3, thread4;
	void* dummy;

	pthread_create(&thread1, 0, cache_test5_thread, 0);
	pthread_create(&thread2, 0, cache_test5_thread, 0);
	pthread_create(&thread4, 0, cache_test5_thread, 0);
	pthread_create(&thread3, 0, cache_test5_thread2, 0);
	pthread_join(thread1, &dummy);
	pthread_join(thread2, &dummy);
	pthread_join(thread3, &dummy);
	pthread_join(thread4, &dummy);
}

static void cache_test6(void) {
	word_t addr, ts, i;
	cache_t content;

	for (addr = cache_start; addr < cache_end; addr += sizeof(cache_t)) {
		ts = cache_read(addr, content);
		if (ts == max_word)
			printf("%d: inaccessible %ld (6)\n", mpi_rank, addr);
		else if (ts!=3 || content[0]!=3 || content[line_size-1]!=3) {
			printf("%d: inconsistent %ld (6): %ld %ld %ld\n",
			       mpi_rank, addr, ts, content[0],
			       content[line_size - 1]);
			for (i = search_start(addr); i < search_end(addr); ++i) {
				printf("%ld %ld\n", meta[i].addr, meta[i].ts);
			}
		}
	}
}

static void cache_test7(void) {
	word_t ptr;
	for (ptr = 4096; ptr < mem_count * sizeof(cache_t);
	     ptr += sizeof(cache_t)) {
		if (cache_home(ptr) != dir_first(ptr))
			debug("%d: mismatch home for %ld\n", mpi_rank, ptr);
	}
}

void trc_cache_test(void) {
	info("%d: cache self testing...\n", mpi_rank);
	cache_test1();
	cache_test2();
	cache_test3();
	cache_test4();
	cache_test5();
	cache_test6();
	cache_test7();
	info("%d: cache testing done\n", mpi_rank);
}

/* Init and terminate functions */
static void cache_init(void) {
	word_t i, j, start, end;
	cache_t empty;

	/* Allocate the memory */
	meta = malloc(sizeof(meta_t) * line_count);
	cache = malloc(sizeof(cache_t) * line_count);
	cache_start = (mem_count * mpi_rank) / mpi_size * sizeof(cache_t);
	cache_end = (mem_count * (mpi_rank + 1)) / mpi_size * sizeof(cache_t);
	if (!cache_start) cache_start = 4096;
	memset(empty, 0, sizeof(cache_t));

	/* Initialize the meta content */
	for (i = 0; i < line_count; ++i) {
		meta[i].addr = max_word_bit;
	}

	/* Initialize the cache and directory */
	for (i = 0; i < mpi_size; ++i) {
		start = (mem_count * i) / mpi_size * sizeof(cache_t);
		end = (mem_count * (i + 1)) / mpi_size * sizeof(cache_t);
		if (!start) start = 4096;
		cache_neighbour[i] = start;

		if (i != mpi_rank) {
			for (j = start; j < end; j += sizeof(cache_t)) {
				dir_clear(j);
				dir_add(j, i);
				dir_set(j, INVALID);
			} 
		} else {
			for (j = start; j < end; j += sizeof(cache_t)) {
				dir_clear(j);
				dir_add(j, i);
				dir_set(j, WITHDRAWN);
				cache_deposit(j, 0, empty);
			}
		}
	}
	cache_neighbour[i + 1] = mem_count * sizeof(cache_t);
	
	info("%d: cache initialised (%ld...%ld)\n",
	     mpi_rank, cache_start, cache_end);
}

static void cache_exit(void) {
	free((void*)cache);
	free((void*)meta);
}

