/*
Copyright (c) 2011-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

void trc_begin(roll_t* roll, hint_t* hint) {
	tx_t* tx;
	tx = rwset_locate();
	if (++(tx->nest) > 1) return;
	tx->roll = roll;
	tx->hint = hint;
	tx->read_only = 1;
	tx->mode = LA;
	// debug("%d: begin\n", mpi_rank);
	timestamp_get(tx->clocks);
}

void trc_abort_aa(tx_t* tx) {
	tx->nest = 0;
	rwset_unlock_all(tx);
	rwset_clear(tx);
	// debug("%d: abort\n", mpi_rank);
	siglongjmp(*(tx->roll), 0);
}

void trc_abort_ea(tx_t* tx) {
	tx->nest = 0;
	rwset_unlock(tx);
	rwset_clear(tx);
	siglongjmp(*(tx->roll), 0);
}

void trc_abort_la(tx_t* tx) {
	tx->nest = 0;
	rwset_clear(tx);
	siglongjmp(*(tx->roll), 0);
}

void trc_abort(void) {
	tx_t* tx;
	tx = rwset_locate();
	switch (tx->mode) {
	case LA: return trc_abort_la(tx);
	case EA: return trc_abort_ea(tx);
	case AA: return trc_abort_aa(tx);
	default: printf("%d: unknown mode\n", mpi_rank); abort();
	}
}

void trc_commit_aa(tx_t* tx) {
	word_t ts;
	if (tx->nest <= 0) return;
	if (--(tx->nest) >= 1) return;
	ts = timestamp_new();
	rwset_release_all(tx, ts);
	// debug("%d: commit\n", mpi_rank);
	rwset_clear(tx);
}

void trc_commit_ea(tx_t* tx) {
	word_t ts;
	if (tx->nest <= 0) return;
	if (--(tx->nest) >= 1) return;
	if (!tx->read_only) {
		if (!rwset_check(tx)) trc_abort_ea(tx);
		ts = timestamp_new();
		rwset_release(tx, ts);
	}
	rwset_clear(tx);
}

static void trc_commit_la(tx_t* tx) {
	word_t ts;
	if (tx->nest <= 0) return;
	if (--(tx->nest) >= 1) return;
	if (!tx->read_only) {
		if (!rwset_lock(tx)) trc_abort_la(tx);
		if (!rwset_check(tx)) trc_abort_ea(tx);
		ts = timestamp_new();
		rwset_release(tx, ts);
	}
	rwset_clear(tx);
}

void trc_commit(void) {
	tx_t* tx;
	tx = rwset_locate();
	switch (tx->mode) {
	case LA: return trc_commit_la(tx);
	case EA: return trc_commit_ea(tx);
	case AA: return trc_commit_aa(tx);
	default: printf("%d: unknown mode\n", mpi_rank); abort();
	}
}

